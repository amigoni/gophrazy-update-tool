
exports.GetPhrases = function(){
	var phrases = globals.col.phrases.find({text:{$ne: "niente"}, deleted: false},{$sort:{createdDate:3}});
	return phrases;
};


exports.AddPhrase = function(data){
	var phrases = JSON.parse(JSON.stringify(globals.col.phrases.find({text:{$ne: "niente"}},{$sort:{phraseID:-1}})));
	var nextPhraseID = 0;
	if(phrases.length > 0) nextPhraseID = phrases[0].phraseID+1;
	if(data.type == "fortuneCookie" && (data.author == "" || data.author == null)) data.author = "Gino the Baker";
	
	globals.col.phrases.save({
		phraseID: nextPhraseID,
		text: data.text,
		author: data.author,
		description: data.description,
		createdDate: new moment().toISOString(),
		releaseID: data.releaseID,
		releaseDate: data.releasedDate,
		released: data.released,
		type: data.type,
		includeInGeneral: BooleanConverter(data.includeInGeneral),
		isFunny: BooleanConverter(data.isFunny),
		groupID: data.groupID,
		numberOfWords: data.numberOfWords,
		difficulty: data.numberOfWords,
		deleted: false
	});
	
	
	if(globals.importingData == false) {
		globals.col.phrases.commit();
		return globals.col.phrases.find({$id: globals.col.phrases.getLastInsertId()})[0];
	}	
};


exports.DeletePhrase = function(phraseID){
	var GroupsController = require("controller/GroupsController");
	var ReleasesController = require("controller/ReleasesController");
	
	var phrase = globals.col.phrases.find({phraseID: phraseID})[0];
	phrase.deleted = true;
	phrase.groupID = null;
	phrase.releaseID = null;
	phrase.released = "false";
	
	GroupsController.AddPhraseToGroup(phrase, phrase.groupID, null);
	ReleasesController.AddPhraseToRelease(phrase, phrase.releaseID, null);
};


exports.ImportPhrases = function(){
	var data = JSON.parse(createDataFiles.GetJsonText("sources/"+globals.i18n+"/Phrases/PhrasesDB_1.txt")); 

 	for(var i = 0; i < data.length; i++){
 		//data[i].createdDate = "2014-01-01T12:00Z";
 		//data[i].released = "true";
 		//data[i].releaseDate = "2014-01-01T12:00Z";
 		if(data[i].groupID == "main" || data[i].groupID == "fortune_cookie" || data[i].groupID == "tutorial") data[i].groupID = null;
 		else if(data[i].groupID == "issue_1") data[i].groupID = 0;
 		else if(data[i].groupID == "issue_2") data[i].groupID = 1;
 		else if(data[i].groupID == "issue_3") data[i].groupID = 2;
 		else if(data[i].groupID == "issue_4") data[i].groupID = 3;
 		
 		
 		data[i].released = "staged";
 		data[i].releaseID = 0;
 		exports.AddPhrase(data[i]);
 		Ti.API.info(i);
 	}
 	
 	Ti.API.info("Imported Phrases");
 	
 	globals.col.phrases.commit();
 	
 	var phrases = globals.col.phrases.find({phraseID: {$in:[0,1,2,3]}});
 	for(var i=0; i < phrases.length; i++) GroupsController.AddPhraseToGroup(phrases[i], null, phrases[i]);
};


function BooleanConverter(value){
	var returnValue = value;
	if(value == 1 || value == "TRUE" || value == "True" || value == "true") returnValue = true;
	else if (value == 0 || value == "FALSE" || value == "False" || value == "false") returnValue = false;
	return returnValue;
}