//Wrapper for Real Time Storage
var applicationKey ="MiNrWy";
var privateKey = "sDhlyoJv9Zlj";
var authenticationToken = "normalUser";

function MakeCall (method, object, callback){
	var url = 'https://storage-backend-prd-useast1.realtime.co/';
	var client = Ti.Network.createHTTPClient({
	     // function called when the response data is available
	     onload : function(e) {
	         callback({text:this.responseText, status: this.statusText});
	     },
	     // function called when an error occurs, including a timeout
	     onerror : function(e) {
	     	callback(e);
	     },
	     timeout : 10000  // in milliseconds
	 });
	 
	 // Prepare the connection.
	client.setRequestHeader("Content-Type", "application/json");
	client.setRequestHeader('charset', 'utf-8');
	
	client.open("POST", url+method);
	client.send(JSON.stringify(object));
}


exports.queryItems = function(table, key, properties, filter, startKey, limit, searchForward, callback){
	var object = {};
	object.applicationKey = applicationKey;
	//if (privateKey!= null) object.privateKey = privateKey;
	if (authenticationToken != null) object.authenticationToken = authenticationToken;
	object.table = table;
	object.key = key;
	if(properties) object.properties = properties;
	if(filter) object.filter = filter;
	if(startKey) object.startKey = startKey;
	if(limit) object.limit = limit;
	if(searchForward) object.searchForward = searchForward;
	object.callback = callback;
	
	MakeCall("queryItems", object, callback);	
};


exports.listItems = function(table, properties, filter, startKey, limit, callback){
	var object = {};
	object.applicationKey = applicationKey;
	//if (privateKey!= null) object.privateKey = privateKey;
	if (authenticationToken != null) object.authenticationToken = authenticationToken;
	object.table = table;
	if(properties) object.properties = properties;
	if(filter) object.filter = filter;
	if(startkey) object.startKey = startKey;
	if(limit) object.limit = limit;
	object.callback = callback;
	
	MakeCall("listItems", object, callback);	
};


exports.putItem = function (table, item, callback){
	var object = {};
	object.applicationKey = applicationKey;
	//if (privateKey!= null) object.privateKey = privateKey;
	if (authenticationToken != null) object.authenticationToken = authenticationToken;
	object.table = table;
	object.item = item;
	object.callback = callback;
	//Ti.API.info(object);
	
	MakeCall("putItem", object, callback);
};


exports.authenticate = function(authenticationToken, timeout, roles, policies, callback){
	 if (timeout == "never") timeout = 9999999999; //~300 years;
	 var object = {
		applicationKey: applicationKey,
		privateKey: privateKey,
		// The token name
		authenticationToken: authenticationToken,
		// The token is valid for 900 seconds
		timeout: timeout,
		// The token will inherit the policies of the ReadOnly role
		roles: roles,//["ReadOnly"],
		// The additional update policy for ProductCatalog table
		policies: policies//{tables: {"ProductCatalog": { allow: "U"}}}             
	}; 
	
	MakeCall("authenticate", object, callback);
};


exports.queryItems("PuzzlesMessages", {primary: "1234567"}, null, null, null, null, false, function(e){
	Ti.API.info(e);
});


/*
putItem("PuzzlesMessages", {receiverID: "Ciao2", messageID: "Bella2"},function(e){
	Ti.API.info(e);
});
*/

/*
listItems("PuzzlesMessages", null, {operator: "equals", item: "receiverID", value: "1234567" }, null, null, function(e){
	Ti.API.info(e);
});

*/

/*
authenticate(
	"[UserFacebookID]", 
	"never", 
	["normalUser"], 
	{items:[{key: {primary: “B00EP1D35O”}, allow: “U” }]},
	function(e){Ti.API.info(e)}
);
*/


