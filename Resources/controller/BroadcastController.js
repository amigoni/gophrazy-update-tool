
//Custum Push Notification text max seems to be 82 chars with global channel but pay attention to chanel length.
exports.BroadcastCustomMessage = function(message, channel, callback){
	var url = 'https://ortc-mobilepush.realtime.co/mp/publish';
	
	var notification =  {
	 	applicationKey: "OMDicR",
	 	privateKey: "EwJLgICnNRdF", 
	 	channel : channel, 
	 	message : message,
	 	//"payload" : { "sound" : "default", "badge" : "1"}
	 };
	
	Ti.API.info(message.length); 
	Ti.API.info(JSON.stringify(notification)); 
	 
	var client = Ti.Network.createHTTPClient({
	     onload : function(e) {
	         Ti.API.info("Received text: " + this.responseText);
	         callback("Success");
	     },
	     onerror : function(e) {
	         Ti.API.debug(e);
	         callback(e);
	     },
	     timeout : 10000
	 });
	 
	 // Prepare the connection.
	client.setRequestHeader("Content-Type", "application/json");
	client.setRequestHeader('charset', 'utf-8');
	
	client.open("POST", url);
	client.send(JSON.stringify(notification));
};


// Push notifications just says "New Message". message can be as long as you want really.
exports.BroadcastNormalMessage = function(message, channel){
	var ortc = require('/lib/co.realtime.ortc.apns');
	ortc.connectionMetadata = 'WordHop';
	ortc.clusterUrl = 'http://ortc-developers.realtime.co/server/2.1';
	var realChannel = channel;
	//ortc.url = 'http://ortc-developers-euwest1-S0001.realtime.co/server/2.1';
	ortc.addEventListener('onConnected', function(e) {
		Ti.API.info("ORTC: Connected: "+e);
		ortc.subscribe(realChannel,true);
	});
	
	ortc.addEventListener('onSubscribed', function(e) { 
		Ti.API.info("ORTC: Subscribed to: "+e.channel+" "+e);
		Ti.API.info(message.length);
		ortc.send(realChannel, message);
	});
	
	ortc.addEventListener('onMessage', function(e) {
		Ti.API.info('ORTC: Received Message - Channel: '+e.channel+'- Message: '+e.message);
	});
	
	ortc.connect("OMDicR");
};

//Max 82 Chars with this channel 
//exports.broadcastCustom("There are so many characters in this sentence that it's important to try to fitial", "GoPhrazyGlobal_en");

//Seems like the channels can only be alpha not numeric so converting numbers to letters.
function ChannelConverter(dataIn){
	var returnString = "";
	for( var i = 0; i < dataIn.length; i++){
		var c = dataIn[i];
		if (c == 0){ returnString += "a"; }
		else if(c == 1){ returnString += "b"; }
		else if(c == 2){ returnString += "c"; }
		else if(c == 3){ returnString += "d"; }
		else if(c == 4){ returnString += "e"; }
		else if(c == 5){ returnString += "f"; }
		else if(c == 6){ returnString += "g"; }
		else if(c == 7){ returnString += "h"; }
		else if(c == 8){ returnString += "i"; }
		else if(c == 9){ returnString += "j"; }
	}
	Ti.API.info(returnString);
	
	return returnString;
}
