exports.GetReleases = function (type){
	var releases;
	if (type == "unreleased") releases = globals.col.releases.find({releaseID: {$ne:"niente"}, released: false}, {$sort:{releaseID:-1}});
	else releases = globals.col.releases.find({releaseID:{$ne:"niente"}}, {$sort:{releaseID:-1}});
	return releases;
};


exports.AddRelease = function(data){
	var releases = JSON.parse(JSON.stringify(exports.GetReleases()));
	var nextReleaseID = 0;
	if(releases.length > 0 ) nextReleaseID = releases[0].releaseID+1;
		
	
	globals.col.releases.save({
		releaseID: nextReleaseID,
		released: false,
		releaseDate: null, 
		createdDate: new moment().toISOString(), 
		mainPhrasesIDs: data.mainPhrasesIDs == null ? [] : data.mainPhrasesIDs,
		fortunePhrasesIDs: data.fortunePhrasesIDs == null ? []: data.fortunePhrasesIDs, 
		groupsIDs: []
	});
	
	globals.col.releases.commit();	
};


exports.DeleteRelease = function(){
	
};


exports.AddGroupToRelease = function(groupID, oldReleaseID, newReleaseID){
	var oldRelease = globals.col.releases.find({releaseID: oldReleaseID})[0];
	var release = globals.col.releases.find({releaseID: newReleaseID})[0];
	
	//Remove from Old
	if(oldReleaseID != null){
		for(var i = 0; i < oldRelease.groupsIDs.length; i++){
			if(oldRelease.groupsIDs == groupID) oldRelease.groupsIDs.splice(i,1);
			break;
		}
	}
	
	//Add to New
	if(newReleaseID != null){
		var groupPresent = false;
		for(var i = 0; i < release.groupsIDs.length; i++){
			if(release.groupsIDs == groupID) groupPresent = true;
			break;
		}
		if(groupPresent == false) release.groupsIDs.push(groupID);
	}
	
	globals.col.releases.commit();
};


exports.AddPhraseToRelease = function(phrase, oldReleaseID, newReleaseID){
	var oldRelease = globals.col.releases.find({releaseID: oldReleaseID})[0];
	var release = globals.col.releases.find({releaseID: newReleaseID})[0];
	
	//Remove from Old
	if(oldReleaseID != null){
		for(var i = 0; i < oldRelease.fortunePhrasesIDs.length; i++){
			if(oldRelease.fortunePhrasesIDs == phrase.phraseID) oldRelease.fortunePhrasesIDs.splice(i,1);
			break;
		}
	
		for(var i = 0; i < oldRelease.mainPhrasesIDs.length; i++){
			if(oldRelease.mainPhrasesIDs == phrase.phraseID) oldRelease.mainPhrasesIDs.splice(i,1);
			break;
		}	
	}
	
	//Add to New
	if(newReleaseID != null){
		var phrasePresent = false;
		
		if(phrase.type == "fortuneCookie"){
			for(var i = 0; i < release.fortunePhrasesIDs.length; i++){
				if(release.fortunePhrasesIDs == phrase.phraseID) phrasePresent = true;
				break;
			}
			
			if(phrasePresent == false) release.fortunePhrasesIDs.push(phrase.phraseID);
		}
		else if(phrase.includeInGeneral == true){
			for(var i = 0; i < release.mainPhrasesIDs.length; i++){
				if(release.mainPhrasesIDs == phrase.phraseID) phrasePresent = true;
				break;
			}
			
			if(phrasePresent == false) release.mainPhrasesIDs.push(phrase.phraseID);
		}
	}
	
	Ti.API.info("SAVING RELEASE");
	Ti.API.info(release);
	
	globals.col.releases.commit();
};


exports.PublishRelease = function(releaseID){
	var createDataFiles = require("controller/createDataFiles/createDataFiles");
	var release = globals.col.releases.find({releaseID: releaseID})[0];
	var now = new moment().toISOString();
	
	release.releaseDate = now;
	release.released = true;
	
	//Change Groups
	var groups = globals.col.groups.find({releaseID: releaseID});
	
	for (var i = 0; i < groups.length; i++){
		groups[i].releaseDate = now;
		groups[i].released = "true";
	}
	
	//Change Phrases
	var phrases = globals.col.phrases.find({releaseID: releaseID});
	
	for (var i = 0; i < phrases.length; i++){
		phrases[i].releaseDate = now;
		phrases[i].released = "true";
	}
	
	globals.col.phrases.commit();
	globals.col.groups.commit();
	globals.col.releases.commit();
	
	createDataFiles.Init(releaseID);
	
	return release;
};
