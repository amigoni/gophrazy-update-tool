exports.InitData = function(){
	var PhrasesController = require("controller/PhrasesController");
	var GroupsController = require("controller/GroupsController");
	
	var mySK = KeyGen();
	var jsondb = require("lib/com.irlgaming.jsondb");
	jsondb.debug(false);
	
	var locale = Titanium.Platform.locale;
	
	var newFolder = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, globals.i18n);
	if( !newFolder.exists() )  newFolder.createDirectory();
	var languageFolder = newFolder.resolve();
	
	function KeyGen(){
		var one = "uno";
		var two = "due";
		return Ti.Utils.base64encode(one+two);
	}
	
	globals.col = {};
	globals.col.appData = jsondb.factory('appData', mySK);
	globals.col.phrases = jsondb.factory('phrases', mySK, languageFolder);
	globals.col.phrases.ensureIndex({phraseID:1});
	globals.col.groups = jsondb.factory('groups', mySK, languageFolder);
	globals.col.releases = jsondb.factory("releases", mySK, languageFolder);
	
	globals.importingData = false;
	
	if(globals.importingData == true){
		PhrasesController.ImportPhrases();
		GroupsController.ImportGroups();
	}
	
	
	//Resetting various
	/*
	var release = globals.col.releases.find({releaseID: 0})[0];
	var phrases = globals.col.phrases.getAll();
	
	for(var i = 0; i < phrases.length; i++){
		var phrase = phrases[i];
		
		if(phrase.releaseID == 0){
			if(phrase.includeInGeneral == true) release.mainPhrasesIDs.push(phrase.phraseID);
			if(phrase.type == "fortuneCookie") release.fortunePhrasesIDs.push(phrase.phraseID);
			phrase.released = "staged";
		} 
		
		else{
			phrase.releaseID = null;
			phrase.released = "false";
			phrase.groupID = null;
		}
		
	}
	
	globals.col.phrases.commit();
	*/
	
	/*
	var phrases = globals.col.phrases.getAll();
	for(var i = 0; i < phrases.length; i++){
		phrases[i].releaseID = 0;
		phrases[i].released = "staged";	
	}
	globals.col.phrases.commit();
	*/
};
