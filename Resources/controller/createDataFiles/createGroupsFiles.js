var createDataFiles = require("controller/createDataFiles/createDataFiles");

exports.CreateGroupPhrasesFiles = function(){
	var GroupsController = require("controller/GroupsController");
	//globals.createDataFiles.groups = JSON.parse(createDataFiles.GetJsonText("sources/"+globals.language+"/Groups/GroupsData_"+globals.createDataFiles.currentVersion.groupsVersion+".txt"));
	globals.createDataFiles.groups = JSON.parse(JSON.stringify(globals.col.groups.find({releaseID: {$lte: globals.currentContentReleaseNumber}, deleted: false}, {$sort:{createdDate:3}})));
	Ti.API.info("Total Groups: "+globals.createDataFiles.groups.length);
	var groups = globals.createDataFiles.groups;
	var phrases = globals.createDataFiles.phrases;
	
	//Groups Phrases files to download when you buy a group
	var phrasesGroupsFilesArray = [];
	for (var i = 0; i < groups.length; i++){
		phrasesGroupsFilesArray.push({
			groupID: groups[i].groupID,
			releaseID: groups[i].releaseID,
			phrases: []
		});
	}
	//Ti.API.info(phrasesGroupsFilesArray);
	for (var i = 0; i < phrases.length; i++){
		var phrase = phrases[i];
		if (phrase.groupID != null){
			for (var j = 0; j < phrasesGroupsFilesArray.length; j++){
				var group = phrasesGroupsFilesArray[j];
				if (phrase.groupID == group.groupID){
					group.phrases.push(phrase);
					break;
				}
			}
		}	
	}
	
	//Ti.API.info(phrasesGroupsFilesArray);
	var groupsDifficultiesSum = 0;
	
	for (var i = 0; i < phrasesGroupsFilesArray.length; i++){
		var group = phrasesGroupsFilesArray[i];
		
		//Difficulty Calculation
		var difficultyTotal = 0;
		var avgDifficulty;
		for(var k = 0; k < group.phrases.length; k++){
			difficultyTotal += group.phrases[k].difficulty;
		}
		
		avgDifficulty = Math.floor(difficultyTotal/group.phrases.length);
		group.avgDifficulty = avgDifficulty;
		groupsDifficultiesSum += group.avgDifficulty;
		//
		
		Ti.API.info("Phrases Groups: "+group.groupID+" Phrases: "+group.phrases.length+" Avg. Difficulty: "+group.avgDifficulty);
		if(group.releaseID >= globals.currentContentReleaseNumber) createDataFiles.WriteFile("group_"+group.groupID+".txt", JSON.stringify(group.phrases));
		
		for(var k = 0; k < groups.length; k++){
			if(groups[k].groupID == group.groupID) groups[k].avgDifficulty = group.avgDifficulty;
		}
	
	}
	
	Ti.API.info("AVG Difficulty of all groups "+(groupsDifficultiesSum/phrasesGroupsFilesArray.length));
	
	exports.CreateGroupListFiles(phrasesGroupsFilesArray);
};
	

exports.CreateGroupListFiles = function(phrasesGroupsFilesArray){
	var ReleasesController = require("controller/ReleasesController");
	///Groups List files
	var releases = globals.col.releases.find({releaseID:{$lte:globals.currentContentReleaseNumber}}, {$sort:{releaseID:1}});
	
	var groupsVersionFilesArray = [];
	for (var i = 0; i < releases.length; i++){
		groupsVersionFilesArray.push({
			number: releases[i].releaseID,
			groups: [],
			phrasesCount: 0
		});
	}
	
	
	var groups = globals.createDataFiles.groups;
	
	for (var i = 0; i < groups.length; i++){
		var group = groups[i];
		group.bought = BooleanConverter(group.bought) ;
		group.locked =  BooleanConverter(group.locked);
		group.featured = BooleanConverter(group.featured);

		//Get PhraseCount from above and inject it.
		for (var j = 0; j < phrasesGroupsFilesArray.length; j++){
			if (phrasesGroupsFilesArray[j].groupID == group.groupID) {
				group.phrasesCount = phrasesGroupsFilesArray[j].phrases.length;

				if(group.cost == "none") group.cost = group.phrasesCount*15;
				else if(group.cost == "free") group.cost = 0;
				else group.cost = parseInt(group.cost);

				break;
			}	
		}

		for (var j = 0; j < groupsVersionFilesArray.length; j++){
			var version = groupsVersionFilesArray[j];
			if (group.releaseID >= version.number){
				version.groups.push(group);
				version.phrasesCount += group.phrasesCount;
			}
		}
	}

	//Write the files
	for (var i = 0; i < groupsVersionFilesArray.length; i++){
		var version = groupsVersionFilesArray[i];
		Ti.API.info("GroupsSinceVersion "+version.number+" Groups: "+version.groups.length+" Phrases: "+version.phrasesCount);
		if(version.number == 0){
			var dir = globals.createDataFiles.release_folder.resolve()+'/AppFiles';
			var folder =Titanium.Filesystem.getFile(dir);
			if(!folder.exists()) folder.createDirectory(); 
			var file1 = Titanium.Filesystem.getFile(
				dir, "GroupsDB.txt"
			);
				
			file1.write(JSON.stringify(version.groups));
		}
		else createDataFiles.WriteFile("GroupsSinceVersion"+version.number+".txt",JSON.stringify(version.groups));
	}
};	


function BooleanConverter(value){
	var returnValue = value;
	if(value == 1 || value == "TRUE" || value == "True" || value == "true") returnValue = true;
	else if (value == 0 || value == "FALSE" || value == "False" || value == "false") returnValue = false;
	return returnValue;
}
		