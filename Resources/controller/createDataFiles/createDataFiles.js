exports.Init = function(releaseID){
	var ReleasesController = require("controller/ReleasesController");
	var createPhrasesFiles = require("controller/createDataFiles/createPhrasesFiles");
	var createGroupsFiles = require("controller/createDataFiles/createGroupsFiles");

	///// Input Area
	globals.language = "en";
	globals.currentContentReleaseNumber = releaseID;
	 
	/////
	globals.createDataFiles = {};
	globals.createDataFiles.currentVersion = {
		appVersion: "1.0",
		phrasesVersion: globals.currentContentReleaseNumber,
		groupsVersion: globals.currentContentReleaseNumber,
		eventsVersion: globals.currentContentReleaseNumber
	};      
	
	
	globals.createDataFiles.new_folder = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, globals.language);
	if( !globals.createDataFiles.new_folder.exists() )  globals.createDataFiles.new_folder.createDirectory();
	
	globals.createDataFiles.release_folder = Titanium.Filesystem.getFile(globals.createDataFiles.new_folder.resolve(), globals.currentContentReleaseNumber);
	if( !globals.createDataFiles.release_folder.exists() )  globals.createDataFiles.release_folder.createDirectory();
	
	globals.createDataFiles.upload_folder = Titanium.Filesystem.getFile(globals.createDataFiles.release_folder.resolve(), "upload");
	if( !globals.createDataFiles.upload_folder.exists() )  globals.createDataFiles.upload_folder.createDirectory();
	
	createPhrasesFiles.CreatePhrasesDB();
	createGroupsFiles.CreateGroupPhrasesFiles();
	
	//Versions start from 1;
	//CurrentVersion.appVersion = "1.1";
	//CurrentVersion.mainPhrasesVersion = phrasesVersionFilesArray.length;
	//CurrentVersion.currentIssueID = issuesSinceFiles.length;
	//CurrentVersion.eventsVersion = eventsVersionFilesArray.length;
	
	exports.WriteFile("CurrentVersion.txt", JSON.stringify(globals.createDataFiles.currentVersion));	
};


exports.GetJsonText = function(fileName){
	var readFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory,fileName);        
 
	if (readFile.exists()) {
		var readContents = readFile.read();
	   	return readContents.text;
	}
};


exports.ParseXcelFile = function(fileName){
	var csvParser = CSVParser.create();
	var returnObject = csvParser.parse(GetJsonText(fileName));
	//Ti.API.info(returnObject.dataGrid);
	return returnObject.dataGrid;
};
	
	
exports.WriteFile = function(fileName, data){
	if(fileName != "CurrentVersion.txt"){
		var file1 = Titanium.Filesystem.getFile(
			globals.createDataFiles.upload_folder.resolve(), fileName
		);
			
		file1.write(data);
		//ZipFile(new_folder,fileName);
		//exports.ZipFile2(fileName);
	}
	else{
		var file1 = Titanium.Filesystem.getFile(
			globals.createDataFiles.upload_folder.resolve(), fileName
		);
			
		file1.write(data);
	}
};
	
	
exports.ZipFile2 = function(fileName){
	var Compression = require('bencoding.zip');
	var outputDirectory = new_folder.resolve()+"/upload";//Ti.Filesystem.applicationDataDirectory;
	var inputDirectory = new_folder.resolve();//Ti.Filesystem.resourcesDirectory + 'data/';
	var writeToZip = outputDirectory + "/"+fileName+'.zip';
	Compression.zip({
    		zip: writeToZip, 
    		//password:"foo",
    		files: [inputDirectory + fileName],
        	completed:onComplete
    });
    
    function onComplete(e){
		//Ti.API.info(JSON.stringify(e));
	};
};

	
exports.ZipFile = function(filePath, fileName){
	var zip = require('net.imthinker.ti.ssziparchive');
     zip.addEventListener('archive', function (e) {
        if (e.success) {
            console.log('ZIP archive is success');
            console.log(e);
        } else {
            console.error('ZIP archive is failure');
            console.error(e);
        }
    });
    
    //Ti.API.info(filePath);
    //var file = Titanium.Filesystem.getFile(filePath, fileName);
    var target = new_folder.resolve()+"/"+fileName+".zip";
    //Ti.API.info(target);
    zip.archive({ // New
        target: target,
        files: [filePath+"/Test.png"],
        overwrite: true
    }); // => success
};