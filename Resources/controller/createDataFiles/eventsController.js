globals.createDataFiles.events =  JSON.parse(createDataFiles.GetJsonText("model/"+language+"/Events/EventsData_"+eventsVersion+"f.txt"));
Ti.API.info("Total Events: "+events.length);
	

function Boh(){
	//Events Phrases files to download when you open an event.
	var eventsFilesArray = [];
	for (var i = 0; i < phrases.length; i++){
		var phrase = phrases[i];
		if (phrase.eventID != "none"){
			var createEvent = true;
			for (var j = 0; j < eventsFilesArray.length; j++){
				var event1 = eventsFilesArray[j];
				if(phrase.eventID == event1.eventID){
					createEvent = false;
					break;
				}
			}
			
			if(createEvent == true){
				var event1 = {
					eventID: phrase.eventID,
					phrases: []
				};
				eventsFilesArray.push(event1);
			}
			
			for (var j = 0; j < eventsFilesArray.length; j++){
				var event1 = eventsFilesArray[j];
				if (phrase.eventID == event1.eventID){
					event1.phrases.push(phrase);
					break;
				}
			}
		}	
	}
	
	for (var i = 0; i < eventsFilesArray.length; i++){
		var event1 = eventsFilesArray[i];
		Ti.API.info("Phrases Event "+event1.eventID+" "+event1.phrases.length);
		WriteFile("event_"+event1.eventID+".txt",JSON.stringify(event1.phrases));
	}
	
}	
	

function Boh2(){
	///Events List Files
	var eventsVersionFilesArray = [];
	//eventsVersionFilesArray.push({number:0, events:[]});
	for (var i = 0; i < events.length; i++){
		var event1 = events[i];
		
		//Get PhraseCount from above and inject it.
		for (var j = 0; j < eventsFilesArray.length; j++){
			if (eventsFilesArray[j].eventID == event1.eventID) {
				event1.phrasesCount = eventsFilesArray[j].phrases.length;
				break;
			}	
		}
		
		// Check to see if you need a new event file.
		var createVersion = true;
		for (var j = 0; j < eventsVersionFilesArray.length; j++){
			var version = eventsVersionFilesArray[j];
			if(event1.eventsVersion-1 == version.number){
				createVersion = false;
				break;
			}
		}
		
		if(createVersion == true){
			var version = {
				number: event1.eventsVersion-1,
				events: [],
				phrasesCount: 0
			};
			eventsVersionFilesArray.push(version);
		}
		//
		
		for (var j = 0; j < eventsVersionFilesArray.length; j++){
			var version = eventsVersionFilesArray[j];
			if (event1.eventsVersion > version.number){
				version.events.push(event1);
				version.phrasesCount += event1.phrasesCount;
			}
		}
	}
	
	for (var i = 0; i < eventsVersionFilesArray.length; i++){
		var version = eventsVersionFilesArray[i];
		Ti.API.info("EventsSinceVersion"+version.number+" Events: "+version.events.length+" Phrases: "+version.phrasesCount);
		WriteFile("EventsSinceVersion"+version.number+".txt",JSON.stringify(version.events));
	}
}	
