var createDataFiles = require("controller/createDataFiles/createDataFiles");

globals.createDataFiles.issues =  JSON.parse(createDataFiles.GetJsonText("model/"+language+"/Issues/IssuesData_"+currentIssueID+"f.txt"));
for (var i = 0; i < globals.createDataFiles.issues.length; i++){
	var issue = globals.createDataFiles.issues;
	issue.collections = [];
	issue.collectionsIDs = [];
	issue.phrasesCount = 0;
	issue.phrasesCost = 0;
}
	

function CreateIssuePhrase(){
	for (var i = 0; i < issues.length; i++){
		var issue = issues[i];
		issue.phrases = [];
		
		for (var j = 0; j < groupsPhrases.length; j++){
			for(var k = 0; k < groups.length; k++){
				if (groups[k].groupID == groupsPhrases[j].groupID){
					groupsPhrases[j].issueID = groups[k].issueID;
					if (groupsPhrases[j].issueID == issue.issueID) issue.phrases.push(groupsPhrases[j]);
				}
			}	
		}
		exports.WriteFile("issues_phrases_"+issue.issueID+".txt", JSON.stringify(issue.phrases));
	}
}


function CreateIssueGroups(){
	///Issue Files with groups but not phrases.
	var issuesSinceFiles = [];
	var phraseAVGValue = 20; //Used to calculate Group Cost
	//issueIDFilesArray.push({number: 0, groups:[]});
	var groups = globals.createDataFiles.groups;
	var issues = globals.createDataFiles.issues;
	for (var i = 0; i < groups.length; i++){
		var group = groups[i];
		group.bought = group.bought == "FALSE" ? false : true;
		group.locked =  group.locked == "FALSE" ? false : true;
		group.featured = group.featured == "FALSE" ? false : true;
		
		//Get PhraseCount from above and inject it.
		for (var j = 0; j < phrasesGroupsFilesArray.length; j++){
			if (phrasesGroupsFilesArray[j].groupID == group.groupID) {
				group.phrasesCount = phrasesGroupsFilesArray[j].phrases.length;
				if(group.cost == "none") group.cost = group.phrasesCount*phraseAVGValue;
				else if(group.cost == "free") group.cost = 0;
				else group.cost = parseInt(group.cost);
				break;
			}	
		}
		
		for (var j = 0; j < issues.length; j++){
			var issue = issues[j];
			if(group.issueID == issue.issueID){
				group.releaseDate = issue.releaseDate;
				issue.groups.push(group);
				issue.groupsIDs.push(group.groupID);
				issue.phrasesCount += group.phrasesCount;
				issue.phrasesCost += group.cost;
			}
		}
	}
	
	for (var i = 0; i < issues.length-1; i++){
		var issue = issues[i];
		issue.cost = Math.floor(issue.phrasesCost*0.7);
		issue.phrases = null; //Erase phrases otherwise it will be in both times.
		
		var issuesArray = [];
		for(var j = 0; j < issues.length; j ++){
			if(new moment(issues[j].releaseDate).valueOf() > new moment(issue.releaseDate)){
				issuesArray.push(issues[j]);
			}
		}
		//Ti.API.info(issuesArray);
		Ti.API.info("IssuesSince_"+issue.issueID+" Groups:"+issue.groups.length+" Phrases:"+issue.phrasesCount);
		exports.WriteFile("IssuesSinceIssue_"+issue.issueID+".txt", JSON.stringify(issuesArray));
	}
	
	exports.WriteFile("AllIssues.txt", JSON.stringify(issues));
}
