var createDataFiles = require("controller/createDataFiles/createDataFiles");


exports.CreatePhrasesDB = function(){
	var ReleasesController = require("controller/ReleasesController");
	//globals.createDataFiles.phrases = JSON.parse(createDataFiles.GetJsonText("sources/"+globals.language+"/Phrases/PhrasesDB_"+globals.createDataFiles.currentVersion.releaseID+".txt")); 
	globals.createDataFiles.phrases = JSON.parse(JSON.stringify(globals.col.phrases.find({releaseID: {$lte: globals.currentContentReleaseNumber}}, {$sort:{createdDate:3}})));
	Ti.API.info("Total Phrases: "+globals.createDataFiles.phrases.length);

	var securely = require('bencoding.securely');
	var stringCrypto = securely.createStringCrypto();
	var allPhrases = [];

	//Main Phrases version;
	var releases = globals.col.releases.find({releaseID:{$lte:globals.currentContentReleaseNumber}}, {$sort:{releaseID:1}});
	var phrasesVersionFilesArray = [];
	for (var i = 0; i < releases.length; i++){
		phrasesVersionFilesArray.push({
			number: releases[i].releaseID,
			phrases: []
		});
	}
	
	
	var phrases = globals.createDataFiles.phrases;
	
	for (var i = 0; i < phrases.length; i++){
		//Process each phrase
		var phrase = phrases[i];
		phrase.text = phrase.text.trim();
		phrase.difficulty = phrase.numberOfWords;
		phrase.numberOfDots = (phrase.text.split(".").length - 1) + (phrase.text.split("?").length - 1) + (phrase.text.split("!").length - 1);
		//Ti.API.info(phrase.numberOfDots);
		if(phrase.numberOfDots == 0) Ti.API.info(phrase.text);
		phrase.text = stringCrypto.AESEncrypt("nutella",phrase.text);
		//phrase.text = stringCrypto.encrypt(phrase.text,"nutella");
			
		if(phrase.released == "true") allPhrases.push(phrase);
	
		for (var j = 0; j < phrasesVersionFilesArray.length; j++){
			var version = phrasesVersionFilesArray[j];
			if (phrase.releaseID >= version.number && (phrase.includeInGeneral == true || phrase.type == "fortuneCookie")) version.phrases.push(phrase);
		}
	}
	
	
	//Write the files
	for (var i = 1; i < phrasesVersionFilesArray.length; i++){
		var version = phrasesVersionFilesArray[i];
		Ti.API.info("PhrasesSinceVersion "+(version.number)+" "+version.phrases.length);
		createDataFiles.WriteFile("PhrasesSinceVersion"+(version.number)+".txt", JSON.stringify(version.phrases));
	}
	
	//Encrypted PhrasesDB for release with the app.
	var dir = globals.createDataFiles.release_folder.resolve()+'/AppFiles';
	var folder =Titanium.Filesystem.getFile(dir);
	if(!folder.exists()) folder.createDirectory(); 
	var file1 = Titanium.Filesystem.getFile(
		dir, "PhrasesDB.txt"
	);
		
	file1.write(JSON.stringify(allPhrases));
};
