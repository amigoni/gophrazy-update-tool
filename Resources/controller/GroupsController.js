exports.GetGroups = function(type){
	var groups;
	if(type == "unreleased") groups = globals.col.groups.find({released:{$ne:"true"}, deleted: false}, {$sort:{createdDate:3}});
	else groups = globals.col.groups.find({name:{$ne:"niente"}, deleted: false}, {$sort:{createdDate:3}});
	return groups;
};


exports.AddGroup = function(data){
	var groups = JSON.parse(JSON.stringify(globals.col.groups.find({name:{$ne:"niente"}}, {$sort:{groupID: -1}})));
	var nextGroupID = 0;
	if(groups.length > 0) nextGroupID = groups[0].groupID+1;
		
	
	globals.col.groups.save({
		groupID: nextGroupID,
		name: data.name,
		description: data.description,
		createdDate: new moment().toISOString(),
		releaseID: globals.importingData == true ? data.releaseID : null,
		releaseDate: null,
		released: globals.importingData == true ? data.released : null,
		publishDate: data.publishDate,
		kind: data.kind,
		cost: data.cost,
		bought: true,
		locked: false,
		featured: false,
		phrasesIDs:[],
		deleted: false
	});
	
	if(globals.importingData == false) globals.col.groups.commit();
	
	return globals.col.groups.find({groupID: nextGroupID })[0];
};


exports.DeleteGroup = function(groupID){
	var group = globals.col.groups.find({groupID: groupID})[0];
	group.deleted = true;
	//Probably should remove all the phrases
	for(var i = 0; i < group.phrasesIDs.length; i++){
		var phrase = globals.col.phrases.find({phraseID: group.phrasesIDs[i]})[0];
		if(phrase != null) phrase.groupID = null;
	}
	
	globals.col.phrases.commit();
	globals.col.groups.commit();
};


exports.AddPhraseToGroup = function (phrase, oldGroupID, newGroupID){	
	var oldGroup = globals.col.groups.find({groupID: oldGroupID})[0];
	var group =  globals.col.groups.find({groupID: newGroupID})[0];
	
	if (newGroupID != null){
		phrase.groupID = newGroupID;
		phrase.released = group.released;
		phrase.releaseID = group.releaseID;
	}
	else{
		phrase.groupID = null;
	}
		
	if(newGroupID != "main"){
		//Remove from old Group
		if(oldGroup != null){
			for(var i = 0; i < oldGroup.phrasesIDs.length; i++){
				if(oldGroup.phrasesIDs[i] == phrase.phraseID) {
					oldGroup.phrasesIDs.splice(i,1);
					break;
				}	
			}
		}
		
		//Add to New
		if(newGroupID != null && group != null){
			var alreadyInGroup = false;
			for(var i = 0; i < group.phrasesIDs.length; i++){
				if(group.phrasesIDs[i] == phrase.phraseID) {
					alreadyInGroup = true;
					break;
				}	
			}
			
			if(alreadyInGroup == false) group.phrasesIDs.push(phrase.phraseID);
		}
	
		globals.col.groups.commit();
	}
	
	if(globals.importingData == false) globals.col.phrases.commit();
};


exports.ImportGroups = function(){
	var data = JSON.parse(createDataFiles.GetJsonText("sources/"+globals.i18n+"/Groups/GroupsData_1.txt"));
 
 	for(var i = 0; i < data.length; i++){
 		data[i].createdDate = "2014-01-01T12:00Z";
 		data[i].released = "staged";
 		//data[i].releaseDate = "2014-01-01T12:00Z";
 		data[i].releaseID = 0;
 		exports.AddGroup(data[i]);
 	}
 	
 	globals.col.groups.commit();
 	
 	Ti.API.info("Imported Groups");
};


function BooleanConverter(value){
	var returnValue = value;
	if(value == 1 || value == "TRUE" || value == "True" || value == "true") returnValue = true;
	else if (value == 0 || value == "FALSE" || value == "False" || value == "false") returnValue = false;
	return returnValue;
}
