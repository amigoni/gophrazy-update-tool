var globals = {};
globals.os = Ti.Platform.osname != "android" ? "ios" : "android";
globals.i18n = "en";
globals.language = "English";

var createDataFiles = require("controller/createDataFiles/createDataFiles");
var RTS = require("controller/RTS");
var CSVParser = require("lib/CSVParser");
var Animator = require("com.animecyc.animator");
var StyleSheet = require("ui/common/StyleSheet");

var moment = require("lib/moment.min");
moment.lang('en', {
    calendar : {
        lastDay : '[Yesterday]',
        sameDay : 'LT',
        nextDay : '[Tomorrow]',
        lastWeek : 'dddd',
        nextWeek : 'dddd',
        sameElse : 'L'
    }
});


(function (){
	var LocalDataController = require("controller/LocalDataController");
	LocalDataController.InitData();
	
	var RootWin = require("ui/RootWin");
	var win = RootWin.createWin();
	win.open();
	
	globals.ui.rootWin.OpenWindow("mainMenuWin");
})();