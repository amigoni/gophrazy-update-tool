exports.loadWin = function(){
	var Window = require("ui/common/Window");
	var NavBar = require("ui/common/NavBar");
	var PhraseDetailWin = require("ui/PhraseDetailWin");
	
	var view = Window.createView();
	var navBar = NavBar.createView("Back", "Phrases", "New");
	
	navBar.leftButton.addEventListener("singletap", function(){
		Close();
		view.fireEvent("closeWin");
	});
	
	navBar.rightButton.addEventListener("singletap", function(){
		globals.ui.rootWin.OpenWindow("phraseDetail", {data:null, isNew:true});
	});
	
	var searchBar = Ti.UI.createTextArea({
		backgroundColor: "transparent",
		borderColor: globals.ui.colors.white,
		borderWidth: "1dp",
		width: "280dp",
		height: "35dp",
		top: "44dp",
		color: globals.ui.colors.white,
		font: globals.ui.fonts.hel12r,
		value: "",
		autocapitalization: true,
		autocorrect: false
	});
	
	searchBar.addEventListener("change", function(e){
		if(e.value.length > 2) {
			autoCompleteTableView.visible = true;
			autoCompleteTableView.update(e.value);
		}
		else{
			autoCompleteTableView.visible = false;
		}	
	});
	
	var tableView = CreateTableView();
	
	var autoCompleteTableView = PhraseDetailWin.CreateAutoCompleteTableView(globals.col.phrases);
	autoCompleteTableView.top = "80dp";
    autoCompleteTableView.width = "320dp";
    autoCompleteTableView.height = "190dp";
    
	autoCompleteTableView.addEventListener("singletap", function(e){
		globals.ui.rootWin.OpenWindow("phraseDetail", {data: e.rowData.data, isNew: false});
		autoCompleteTableView.visible = false;
	});
	
	
	view.add(navBar);
	view.add(searchBar);
	view.add(tableView);
	view.add(autoCompleteTableView);
	
	function Close(){
		tableView.Close();
	}
	
	
	return view;
};


function CreateTableView(){
	var Common = require("ui/common/Common");
	var PhrasesController = require("controller/PhrasesController");
	var data = PhrasesController.GetPhrases();
	
	
	var tableData = [];
	
	var view = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		separatorColor: globals.ui.colors.white,
		height: (globals.platform.actualHeightNumber-80).toString()+"dp",
		top: "80dp"
	});
	
	//Common.MakeDynamicTableView(view,)
	
	function Refresh(){
		tableData = [];
		for(var i = 0; i < data.length; i++){
			var row = CreateRow(data[i]);
			tableData.push(row);
		}
		
		view.data = tableData;
	}
	
	
	view.Refresh = Refresh;
	
	Refresh();
	
	
	function UpdatePhrase(data){
		for(var i = 0; i < tableData.length; i++){
			if(tableData[i].data.$id == data.$id){
				tableData[i].Update();
			}
		}
	}
	
	function AddPhrase(data){
		tableData.unshift(CreateRow(data.data));
		view.data = tableData; 
	}
	
	function DeletePhrase(){
		data = PhrasesController.GetPhrases();
		Refresh();
	}
	
	function Close(){
		Ti.App.removeEventListener("updatePhrase", UpdatePhrase);
		Ti.App.removeEventListener("addPhrase", AddPhrase);	
		Ti.App.removeEventListener("deletePhrase", DeletePhrase);
	}
	
	
	view.Close = Close;
	
	Ti.App.addEventListener("updatePhrase", UpdatePhrase);	
	Ti.App.addEventListener("addPhrase", AddPhrase);
	Ti.App.addEventListener("deletePhrase", DeletePhrase);	
	
	
	return view;
}


function CreateRow(data){
	var SquareButton = require("ui/common/SquareButton");
	
	var view = Ti.UI.createTableViewRow({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height: "50dp",
		selectionStyle: 2,
		data: data
	});
	
	
	var container1 = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	container1.addEventListener("singletap", function(){
		//SwitchView();
		globals.ui.rootWin.OpenWindow("phraseDetail", {data: view.data, isNew: false});
	});
	
	var square = Ti.UI.createView({
		backgroundColor: globals.ui.colors.white,
		width: "3dp",
		height: "46dp",
		left: "4dp",
		top: "2dp"
	});
	
	var textLabel = Ti.UI.createLabel({
		width: "260dp",
		height: Ti.UI.SIZE,
		top: "2dp",
		left: "15dp",
		text: data.text,
		textAlign: "left",
		font: globals.ui.fonts.hel12r,
		color: globals.ui.colors.white
	});
	
	var idLabel = Ti.UI.createLabel({
		width: "30dp",
		height: Ti.UI.SIZE,
		top: "2dp",
		right: "10dp",
		text: data.phraseID,
		textAlign: "right",
		font: globals.ui.fonts.hel9r,
		color: globals.ui.colors.white
	});
	
	var authorLabel = Ti.UI.createLabel({
		width: "280dp",
		height: Ti.UI.SIZE,
		bottom: "2dp",
		right: "10dp",
		text: data.author,
		textAlign: "right",
		font: globals.ui.fonts.hel11b,
		color: globals.ui.colors.white
	});
	
	
	container1.add(square);
	container1.add(textLabel);
	container1.add(idLabel);
	container1.add(authorLabel);
	
	var buttonView; 
	
	view.add(container1);
	
	
	function Update(){
		view.data = globals.col.phrases.find({phraseID: view.data.phraseID})[0];
		Refresh(view.data);
	}
	
	function Refresh(newData){
		if(newData.released == "true") square.backgroundColor = globals.ui.colors.green;
		else if(newData.released == "false") square.backgroundColor = globals.ui.colors.red;
		else if(newData.released == "staged") square.backgroundColor = globals.ui.colors.orange;
		
		textLabel.text = newData.text;	
		authorLabel.text = newData.author;
	}
	
	function SwitchView(){
		container1.opacity = 0;
		buttonView = CreateButtonView();
		view.add(buttonView);
		
		setTimeout(function(){
			buttonView.opacity = 0;
			buttonView.Close();
			view.remove(buttonView);
			buttonView = null;
			container1.opacity = 1;
		},3000);
	}
	
	function CreateButtonView(){
		var buttonView = Ti.UI.createView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			layout: "horizontal"
		});
		
		var editButton = SquareButton.createView("Edit");
		editButton.addEventListener("singletap", function(){
			globals.ui.rootWin.OpenWindow("phraseDetail", {data: view.data, isNew: false});
		});
		
		var addToMainButton = SquareButton.createView("Add to\nMain");
		addToMainButton.left = "10dp";
		addToMainButton.addEventListener("singletap", function(){
			alert("AddToMain");
		});
		
		var addToFortuneButton = SquareButton.createView("Add to \nFortune");
		addToFortuneButton.left = "10dp";
		addToFortuneButton.addEventListener("singletap", function(){
			alert("AddToFavorite");
		});
		
		var deleteButton = SquareButton.createView("Delete");
		deleteButton.left = "10dp";
		deleteButton.addEventListener("singletap", function(){
			alert("Delete");
		});
		
		buttonView.add(editButton);
		buttonView.add(addToMainButton);
		buttonView.add(addToFortuneButton);
		buttonView.add(deleteButton);
		
		function Close(){
			buttonView.remove(editButton);
			buttonView.remove(addToMainButton);
			buttonView.remove(addToFortuneButton);
			buttonView.remove(deleteButton);
			editButton = null;
			addToMainButton = null;
			addToFortuneButton = null;
			deleteButton = null;
		}
		
		buttonView.Close = Close;
		
		return buttonView;
	}
	
	view.Update = Update;
	
	Refresh(view.data);
	
	
	return view;
}
