exports.createWin = function(data){
	var PhraseDetail = require("ui/PhraseDetail");
	
	var win = CommonElements.createStandardWin();
	win.rightNavButton.visible = false;
	win.addEventListener("leftNavButton", function(){
		win.close();
	});
	
	win.addEventListener("rightNavButton", function(){
		
	});
	
	var tableView = Ti.UI.createTableView({
		height: win.height,
		width: win.width
	});
	
	var tableData = [];
	
	for (var i = 0; i < data.length; i++){
		var row = CreateRow(data[i]);
		tableData.push(row);
	}
	
	tableView.data = tableData;


	return win;
};


function CreateRow(rowData){
	var view = Ti.UI.createTableViewRow({
		width: Ti.Platform.displayCaps.platformWidth,
		height: 40,
		title: rowData.text
	});
	
	return view;
}
