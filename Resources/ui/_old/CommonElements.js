exports.createStandardWin = function(){
	var win = Titanium.UI.createWindow({ 
	    orientationModes: [ Titanium.UI.LANDSCAPE_LEFT ],
	    height: Ti.Platform.displayCaps.platformHeight,
	    width: Ti.Platform.displayCaps.platformWidth,
	    top: 0,
	    //left: 0,
	    fullscreen:true,
	    navBarHidden: true,
	    opacity: 1,
	    exitOnClose: true
	});
	
	var navBar = Ti.UI.createView({
		borderColor: "black",
		width: win.width,
		height: 44,
		top: 0
	});
	
	var leftNavButton = Ti.UI.createButton({
		borderColor: "black",
		title: "back",
		width: 60,
		height: 40,
		left: 10
	});
	
	leftNavButton.addEventListener("singletap", function(){
		win.fireEvent("leftNavButton");
	});
	
	var rightNavButton = Ti.UI.createButton({
		borderColor: "black",
		title: "right",
		width: 60,
		height: 40,
		right: 10
	});
	
	rightNavButton.addEventListener("singletap", function(){
		win.fireEvent("rightNavButton");
	});
	
	navBar.add(leftButton);
	navBar.add(rightButton);
	
	
	win.add(navBar);
	
	win.leftNavButton = leftNavButton;
	win.rightNavButton = rightNavButton;
	
	
	return win;
};
