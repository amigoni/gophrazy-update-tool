exports.createWin = function(){
	var win = Titanium.UI.createWindow({ 
	    backgroundColor: "white",
	    orientationModes: [Titanium.UI.LANDSCAPE_LEFT ],
	    width: Ti.Platform.displayCaps.platformHeight,
	    height: Ti.Platform.displayCaps.platformWidth,
	    //top: 0,
	    //left: 0,
	    fullscreen:true,
	    //navBarHidden: true,
	    opacity: 1,
	    exitOnClose: true
	});
	
	var buttonContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	buttonContainer.add(CreateButton("Create Files", CreateDataFiles));
	buttonContainer.add(CreateButton("Broadcast Message", BroadCastMessage));
	
	function CreateButton(title, callback){
		var button = Ti.UI.createLabel({
			borderColor: "black",
			width: 200,
			height: 50,
			top: 20,
			text: title,
			textAlign: "center"
		});
		
		button.addEventListener("singletap", callback);
		
		return button;
	}
	

	
	win.add(buttonContainer);


	return win;
};


function CreateDataFiles(){
	createDataFiles.Init({});
}

function BroadCastMessage(){
	/*
	broadcastMessage.broadcastCustom(
		//LIMITATIONS: MESSAGE 82 CHAR OR TOTAL 179 CHARS
		"Love and compassion",
		"gjdjbaiib"
	);
	*/
	/*
	broadcastMessage.broacastNormal(
		"Love and ",
		"gjdjbaiib"
	);
	*/
}
