exports.createWin = function(data){
	var PhraseDetail = require("ui/PhraseDetail");
	
	var win = CommonElements.createStandardWin();
	win.rightNavButton.visible = false;
	win.addEventListener("leftNavButton", function(){
		win.close();
	});
	
	win.addEventListener("rightNavButton", function(){
		
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: win.width,
		height: win.height-44,
		top: 44,
		contentWidth: win.width,
		contentHeight: win.height-44
	});
	
	var scrollContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		top: 0,
		layout: "vertical"
	});
	
	var topRowView = CreateTopRowView();
	var textView = CreateTextFieldWithAutoComplete("Text: ", null);
	var authorView = CreateTextFieldWithAutoComplete("Author: ", null);
	var collectionView = CreateTextFieldWithAutoComplete("Collection: ", null);
	var eventView = CreateTextFieldWithAutoComplete("Event: ", null);
	


	return win;
};


function CreateTopRowView(data){
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		left: 0,
		layout: "horizontal"
	});
	
	var statusLabel = Ti.UI.createLabel({
		text: data.status,
		height: 30
	});
	
	var phraseIDLabel = Ti.UI.createLabel({
		text: "PhraseID: "+data.phraseID,
		height: 30
	});
	
	var mainPhrasesVersionLabel = Ti.UI.createLabel({
		text: "mainPhrasesVersion: "+data.mainPhrasesVersion,
		height: 30
	});
	
	var createdLabel = Ti.UI.createLabel({
		text: "Created Date: "+data.createdDate,
		height: 30
	});
	
	view.add(statusLabel);
	view.add(phraseIDLabel);
	view.add(mainPhrasesVersionLabel);
	view.add(createdLabel);
	
	
	return view;
};


function CreateStatsView(data){
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		left: 0,
		layout: "horizontal"
	});
	
	var lengthLabel = Ti.UI.createLabel({
		text: data.length,
		height: 30
	});
	
	var numberOfWordsLabel = Ti.UI.createLabel({
		text: data.numberOfWords,
		height: 30
	});
	
	var difficultyTextField = CreateTextFieldWithAutoComplete("Difficulty:", data.difficulty);
	
	var rewardTextField = CreateTextFieldWithAutoComplete("Cost:", data.reward);
	
	
	view.add(lengthLabel);
	view.add(numberOfWordsLabel);
	view.add(difficultyTextField);
	view.add(costTextField);
	
	view.lengthLabel = lengthLabel;
	view.numberOfWordsLabel = numberOfWordsLabel;
	view.difficultyTextField = difficultyTextField;
	view.rewardTextField = rewardTextField;
	
	
	
	return view;
};


function CreateTextFieldWithAutoComplete(title,value){
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		left: 0
	});
	
	var label = Ti.UI.createLabel({
		text: "Text:",
		height: 30
	});
	
	var textField = Ti.UI.createTextField({
		borderColor: "black",
		width: Ti.Platform.displayCaps.platformWidth-300,
		height: 30
	});
	
	view.textField = textField;
	
	return view;
}
