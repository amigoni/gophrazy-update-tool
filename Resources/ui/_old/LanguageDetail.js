exports.createWin = function(){
	var PhraseList = require("ui/PhraseList");
	var CollectionList = require("ui/CollectionList");
	var EventList = require("ui/EventList");
	var ReleaseList = require("ui/ReleaseList");
	
	var win = CommonElements.createStandardWin();
	win.leftNavButton.visible = false;
	win.rightNavButton.visible = false;
	win.addEventListener("leftNavButton", function(){
		win.close();
	});
	
	win.addEventListener("rightNavButton", function(){
		
	});
	
	var buttonContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	buttonContainer.add(CreateButton("Phrases", function(){rootWin.openWindow(PhraseList.createWin());}));
	buttonContainer.add(CreateButton("Collections", function(){rootWin.openWindow(CollectionList.createWin());}));
	buttonContainer.add(CreateButton("Events", function(){rootWin.openWindow(EventList.createWin());}));
	buttonContainer.add(CreateButton("Releases", function(){rootWin.openWindow(ReleaseList.createWin());}));
	
	win.add(buttonContainer);


	return win;
};


function CreateButton(title, callback){
	var button = Ti.UI.createLabel({
		borderColor: "black",
		width: 200,
		height: 50,
		text: title,
		textAlign: "center",
		top: 10
	});
	
	button.addEventListener("singletap", callback);
	
	
	return button;
}