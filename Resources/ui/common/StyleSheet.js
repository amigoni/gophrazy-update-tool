if (globals.os == "ios"){
	globals.imagesPath = "/iphone/images";
	globals.platform = {};
	globals.platform.width = Ti.Platform.displayCaps.platformWidth;
	globals.platform.widthNumber = Ti.Platform.displayCaps.platformWidth;
	globals.platform.height = Ti.Platform.displayCaps.platformHeight;
	globals.platform.heightNumber = Ti.Platform.displayCaps.platformHeight;
	globals.platform.actualHeight = globals.platform.height;
	globals.platform.actualHeightNumber = globals.platform.actualHeight;
	globals.platform.scale = 1.0;
}

globals.ui = {};

globals.ui.colors = {
	black: "#000000",
	white: "#FFFFFF",
	lightGrey: "#AAAAAA",
	green: "#008C23",
	yellow: "#F8BD2E",
	orange: "#D96D00",
	red:"#D80000"
};


globals.ui.fonts = {
	gro10: {fontSize: "10dp", fontFamily:"Groboldov", fontWeight:"regular"},
	gro12: {fontSize: "12dp", fontFamily:"Groboldov", fontWeight:"regular"},
	gro20: {fontSize: "20dp", fontFamily:"Groboldov", fontWeight:"regular"},
	hel9r: {fontSize: "9dp", fontFamily:"Helvetica Neue", fontWeight:"regular"},
	hel12r: {fontSize: "12dp", fontFamily:"Helvetica Neue", fontWeight:"regular"},
	hel11b: {fontSize: "12dp", fontFamily:"Helvetica Neue", fontWeight:"bold"},
};
