exports.createView = function (name, color){
	var view = Ti.UI.createLabel({
		borderColor: globals.ui.colors.white,
		borderWidth: "1dp",
		width: "50dp",
		height: "50dp",
		text: name,
		textAlign: "center",
		font: globals.ui.fonts.gro12,
		color: color == null ? globals.ui.colors.white : color,
		minimumFontSize: "8dp"
	});
	
	return view;
};
