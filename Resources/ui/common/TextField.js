exports.createView = function(labelName, text, height){
	var view = Ti.UI.createView({
		width: "280dp",
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var label = Ti.UI.createLabel({
		width: "280dp",
		height: Ti.UI.SIZE,
		//top: "0dp",
		left: "0dp",
		text: labelName,
		textAlign: "left",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.white
	});
	
	var textArea = Ti.UI.createTextArea({
		backgroundColor: "transparent",
		borderColor: globals.ui.colors.white,
		borderWidth: "1dp",
		width: view.width,
		height: height,
		color: globals.ui.colors.white,
		font: globals.ui.fonts.hel12r,
		value: text
	});
	
	
	view.add(label);
	view.add(textArea);
	
	view.textArea = textArea;
	view.label = label;
	
	
	return view;
};
