exports.createView = function(leftButtonText, titleText, rightButtonText){
	var view = Ti.UI.createView({
		height: "44dp",
		top: "0dp",
		opacity: 1	
	}); 
	
	var leftButton = Ti.UI.createLabel({
		width: "60dp",
		height: "44dp",
		left: "5dp",
		text: leftButtonText,
		textAlign: "center",
		font: globals.ui.fonts.gro20,
		minimumFontSize: "8dp",
		color: globals.ui.colors.yellow
	});
	
	
	leftButton.addEventListener("singletap", function(){
		view.fireEvent("leftPressed");
	});
	
	
	var titleLabel = Ti.UI.createLabel({
		width: "180dp",
		height: "44dp",
		text: titleText,
		textAlign: "center",
		font: globals.ui.fonts.gro20,
		minimumFontSize: "8dp",
		color: globals.ui.colors.white
	});
	
	var rightButton = Ti.UI.createLabel({
		width: "60dp",
		height: "44dp",
		right: "5dp",
		text: rightButtonText,
		textAlign: "center",
		font: globals.ui.fonts.gro20,
		minimumFontSize: "8dp",
		color: globals.ui.colors.yellow
	});
	
	rightButton.addEventListener("singletap", function(){
		view.fireEvent("rightPressed");
	});
	
	
	if(leftButtonText != null) view.add(leftButton);
	view.add(titleLabel);
	if(rightButtonText != null) view.add(rightButton);
	
	view.leftButton = leftButton;
	view.titleLabel = titleLabel;
	view.rightButton = rightButton;
	
	
	return view;
};
