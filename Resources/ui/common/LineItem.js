exports.createView = function(leftText, rightText, type){
	var rightLabelColor = globals.ui.colors.lightGrey;
	if (type != "static") rightLabelColor = globals.ui.colors.yellow;
	
	var value = rightText;
	if(rightText == true && type == "boolean") rightText = "true";
	else if(rightText == false && type == "boolean") rightText = "false";
	
	if(rightText == null) rightText = "null";
	
	var view = Ti.UI.createView({
		//borderColor: "white",
		width: "135dp",
		height: "30dp",
		value: value
	});
	
	var leftLabel = Ti.UI.createLabel({
		width: "70dp",
		height: "30dp",
		//top: "0dp",
		left: "0dp",
		text: leftText,
		textAlign: "right",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.white,
		minimumFontSize: "8dp"
	});
	
	var rightLabel = Ti.UI.createLabel({
		width: "60dp",
		height: "30dp",
		//top: "0dp",
		right: "0dp",
		text: rightText,
		textAlign: "right",
		font: globals.ui.fonts.gro12,
		color: rightLabelColor,
		minimumFontSize: "8dp"
	});
	
	rightLabel.addEventListener("singletap", function(){
		if(type == "boolean"){
			if(view.value == true){
				view.value = false;
				rightLabel.text = "false";
			}
			else if(view.value == false){
				view.value = true;
				rightLabel.text = "true";
			}
		}
	});
	
	
	view.add(leftLabel);
	view.add(rightLabel);
	

	view.leftLabel = leftLabel;
	view.rightLabel = rightLabel;
	
	
	return view;
};
