exports.MakeDynamicTableView = function(tableView, data, loadLimit, CreateRow, CreateBottomRow, CreateTopRow){
	var bottomRow;
	var topRow;
	tableView.loadLimit = loadLimit;
	tableView.nextRowToLoad = 0;
	tableView.lastLoadLastRow = 0;
	tableView.numberViewableRows = 0;
	tableView.numberOfTotalDataRows = data.length;
	
	tableView.addEventListener("more", AddMoreRows);
	//tableView.addEventListener("delete", DeleteRow);
	
	var tableData = [];
	
	function UpdateTableView (){
		tableData = null;
		tableData = [];
		var upperLimit = tableView.loadLimit;
		if (upperLimit > data.length) upperLimit = data.length;
		tableView.numberViewableRows = upperLimit;
		
		if(CreateTopRow){
			tableData.push(CreateTopRow());
			tableView.nextRowToLoad++;
		}
		
		for (var i = 0; i < upperLimit; i++){
			var row = CreateRow(data[i]);
			tableData.push(row);
			tableView.nextRowToLoad++;
		}
		
		if(CreateBottomRow){
			bottomRow = CreateBottomRow(tableView.numberViewableRows, tableView.numberOfTotalDataRows); 
			tableData.push(bottomRow);
		}
		
		tableView.lastLoadLastRow = tableView.nextRowToLoad;
		tableView.data = tableData;
	}
	
	function AddMoreRows (){
		var upperLimit = tableView.nextRowToLoad + tableView.loadLimit;
		if (upperLimit >= data.length) upperLimit = data.length;
		tableView.numberViewableRows = upperLimit;
		
		if (upperLimit > tableView.nextRowToLoad){
			if(CreateBottomRow){
				tableView.deleteRow(tableView.nextRowToLoad,{});
				tableData.splice(tableView.nextRowToLoad,1);
			}
			
			for (var i = tableView.nextRowToLoad; i < upperLimit; i++){
				var newRow = CreateRow(data[i]);
				tableData.push(newRow);
				tableView.appendRow(newRow);
				tableView.nextRowToLoad++;
			}
			
			if(CreateBottomRow){
				var count = tableView.nextRowToLoad;
				if (CreateTopRow) count--;
				bottomRow = CreateBottomRow(count, data.length);
				tableView.appendRow(bottomRow);
				tableData.push(bottomRow);
			}	
			tableView.scrollToIndex(tableView.lastLoadLastRow);
			tableView.lastLoadLastRow = tableView.nextRowToLoad;
		}
	}
	
	function DeleteRow(index){
		tableData.splice(index,1);
		tableView.nextRowToLoad--;
		tableView.numberViewableRows--;
		tableView.numberOfTotalDataRows--;
		tableView.deleteRow(tableView.data[0].rows[index]);
		if(CreateBottomRow) bottomRow.Update(tableView.numberViewableRows, tableView.numberOfTotalDataRows);
	}
	
	function AddRow(newRowData, position){
		tableView.numberViewableRows++;
		tableView.numberOfTotalDataRows++;
		if(CreateBottomRow) bottomRow.Update(tableView.numberViewableRows, tableView.numberOfTotalDataRows);
		tableView.nextRowToLoad++;
		var newRow = CreateRow(newRowData);
		tableData.splice(position,0, newRow);
		tableView.insertRowAfter(position, newRow ,true);
	}
	
	function Close(){
		
	}
	
	tableView.UpdateTableView = UpdateTableView;
	tableView.DeleteRow = DeleteRow;
	tableView.AddRow = AddRow;
	tableView.bottomRow = bottomRow;
	tableView.Close = Close;
	
	UpdateTableView();
};