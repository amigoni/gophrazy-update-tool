exports.loadWin = function(dataIn){
	var Window = require("ui/common/Window");
	var NavBar = require("ui/common/NavBar");
	
	var view = Window.createView();
	var navBar = NavBar.createView("Back", dataIn.name, null);
	
	navBar.leftButton.addEventListener("singletap", function(){
		Close();
		view.fireEvent("closeWin");
	});
	
	navBar.rightButton.addEventListener("singletap", function(){
	
	});
	
	
	var tableView = CreateTableView(dataIn.data, dataIn.callback);
	
	
	view.add(navBar);
	view.add(tableView);
	
	function Close(){
		
	}
	
	
	return view;
};


function CreateTableView(data, callback){
	var Common = require("ui/common/Common");
	
	var tableData = [];
	
	var view = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		separatorColor: globals.ui.colors.white,
		height: (globals.platform.actualHeightNumber-44).toString()+"dp",
		top: "44dp"
	});
	
	//Common.MakeDynamicTableView(view,)
	
	function Refresh(){
		tableData = [];
		for(var i = 0; i < data.length; i++){
			var row = CreateRow(data[i], callback);
			tableData.push(row);
		}
		
		view.data = tableData;
	}
	
	
	view.Refresh = Refresh;
	
	Refresh();
	
	
	return view;
}


function CreateRow(data, callback){
	var view = Ti.UI.createTableViewRow({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height: "40dp",
		selectionStyle: false,
		data: data
	});
	
	//For touch events otherwise it only feels teh label touch.
	var container = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	container.addEventListener("singletap", function(){
		callback(data);
		view.fireEvent("closeWin");
	});
	
	var nameLabel = Ti.UI.createLabel({
		width: "280dp",
		height: Ti.UI.SIZE,
		//top: "5dp",
		left: "20dp",
		text: data.name == null? "null" : data.name,
		textAlign: "left",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.white
	});
	
	container.add(nameLabel);
	view.add(container);
	
	
	return view;
}
