exports.createView = function(){
	var view = Titanium.UI.createView({ 
	    backgroundColor: globals.ui.colors.black,
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    width: globals.platform.width,	
	    height: globals.platform.actualHeight,   
	    top: globals.platform.actualHeight,
	    //opacity: 1
	    bubbleParent: false
	});
	
	view.addEventListener("closeWin", function(){
		Close();
	});
	
	
	//Functions
	function Open(){
		AppearAnimation();
	};
	
	function Close(){
		DisappearAnimation();
	};
	
	function AppearAnimation(){
		setTimeout(function(){
			Animator.animate(view,{
			duration: 500,
			top: "0dp",
			easing:Animator['EXP_OUT']
		});},500);
	};
	
	function DisappearAnimation(){
		Animator.animate(view,{
			duration: 500,
			top: -(globals.platform.actualHeight),
			easing:Animator['EXP_IN']
		},function(){view.fireEvent("closed");});
	};
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};

