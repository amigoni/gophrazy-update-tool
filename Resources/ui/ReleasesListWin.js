exports.loadWin = function(){
	var Window = require("ui/common/Window");
	var NavBar = require("ui/common/NavBar");
	var ReleasesController = require("controller/ReleasesController");

	var view = Window.createView();
	var navBar = NavBar.createView("Back", "Releases", "New");
	
	navBar.leftButton.addEventListener("singletap", function(){
		Close();
		view.fireEvent("closeWin");
	});
	
	navBar.rightButton.addEventListener("singletap", function(){
		//globals.ui.rootWin.OpenWindow("phraseDetail", {isNew: true});
		ReleasesController.AddRelease({isNew: true});
		tableView.Refresh();
	});
	
	
	var tableView = CreateTableView();
	
	
	view.add(navBar);
	view.add(tableView);
	
	function Close(){
		
	}
	
	
	return view;
};


function CreateTableView(){
	var Common = require("ui/common/Common");
	var ReleasesController = require("controller/ReleasesController");
	
	var tableData = [];
	
	var view = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		separatorColor: globals.ui.colors.white,
		height: (globals.platform.actualHeightNumber-44).toString()+"dp",
		top: "44dp"
	});
	
	view.addEventListener("refreshTableView", function(){Refresh();});
	
	//Common.MakeDynamicTableView(view,)
	
	function Refresh(){
		tableData = [];
		var data = ReleasesController.GetReleases();
		for(var i = 0; i < data.length; i++){
			var row = CreateRow(data[i]);
			tableData.push(row);
		}
		
		view.data = tableData;
	}
	
	
	view.Refresh = Refresh;
	
	Refresh();
	
	
	return view;
}


function CreateRow(data){
	var ReleasesController = require("controller/ReleasesController");
	var view = Ti.UI.createTableViewRow({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height: "70dp",
		selectionStyle: false,
		data: data
	});
	
	var statusIndicator = Ti.UI.createView({
		backgroundColor: globals.ui.colors.white,
		width: "3dp",
		height: "66dp",
		left: "4dp",
		top: "2dp"
	});
	
	var idLabel = Ti.UI.createLabel({
		width: "280dp",
		height: Ti.UI.SIZE,
		top: "2dp",
		left: "15dp",
		text: "Release "+data.releaseID,
		textAlign: "left",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.white
	});
	
	var releaseDateLabel = Ti.UI.createLabel({
		width: "150dp",
		height: "15dp",
		top: "2dp",
		right: "80dp",
		text: "RD: "+new moment(data.releaseDate).format("dd, D/M/YY"),
		textAlign: "right",
		font: globals.ui.fonts.hel11b,
		color: globals.ui.colors.white
	});
	
	var createdDateLabel = Ti.UI.createLabel({
		width: "150dp",
		height: "15dp",
		top: "20dp",
		right: "80dp",
		text: "CD: "+new moment(data.createdDate).format("dd, D/M/YY"),
		textAlign: "right",
		font: globals.ui.fonts.hel11b,
		color: globals.ui.colors.white
	});
	
	var mainPhrasesCount = Ti.UI.createLabel({
		width: "100dp",
		height: "15dp",
		top: "20dp",
		left: "15dp",
		text: "Main: "+data.mainPhrasesIDs.length,
		textAlign: "left",
		font: globals.ui.fonts.hel11b,
		color: globals.ui.colors.white
	});
	
	var fortunePhrasesCount = Ti.UI.createLabel({
		width: "100dp",
		height: "15dp",
		top: "35dp",
		left: "15dp",
		text: "Fortune: "+data.fortunePhrasesIDs.length,
		textAlign: "left",
		font: globals.ui.fonts.hel11b,
		color: globals.ui.colors.white
	});
	
	var groupsCount = Ti.UI.createLabel({
		width: "100dp",
		height: "15dp",
		top: "50dp",
		left: "15dp",
		text: "Groups: "+data.groupsIDs.length,
		textAlign: "left",
		font: globals.ui.fonts.hel11b,
		color: globals.ui.colors.white
	});
	
	var releaseButton = Ti.UI.createLabel({
		borderColor: globals.ui.colors.white,
		borderWidth: "1dp",
		width: "50dp",
		height: "25dp",
		top: "5dp",
		right: "10dp",
		text: "Release",
		textAlign: "center",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.white
	});
	
	releaseButton.addEventListener("singletap", function(){
		var message = "";
		if(data.fortunePhrasesIDs.length == 0) message = "There are no Fortune Cookies in this release!";
		else if(data.mainPhrasesIDs.length == 0) message = "There are no Main Phrases in this release!";
		else if(data.groupsIDs.length == 0) message = "There are no Groups in this release!";
		else  message = "All Good!";
		
		var dialog = Ti.UI.createAlertDialog({
	    	cancel: 1,
	    	buttonNames: ['Yes', 'No'],
	    	message: message,
	    	title: 'Release?'
		});
		
		dialog.addEventListener('click', function(e){
		    if (e.index === e.source.cancel) Ti.API.info('The cancel button was clicked');
		    if(e.index == 0){
		    	ReleasesController.PublishRelease(data.releaseID);
				view.fireEvent("refreshTableView");
				alert("Remember to send a broadcast");
		    }
		    else if (e.index == 1){
		    	
		    }
		});
		dialog.show();
	});
	
	var deleteButton = Ti.UI.createLabel({
		borderColor: globals.ui.colors.white,
		borderWidth: "1dp",
		width: "50dp",
		height: "25dp",
		bottom: "5dp",
		right: "10dp",
		text: "Delete",
		textAlign: "center",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.white
	});
	
	deleteButton.addEventListener("singletap", function(){
		if(data.released == true) alert("Shouldn't delete. Already Released!");
		else{
			ReleasesController.DeleteRelease(data.releaseID);
			view.fireEvent("refreshTableView");
		}
	});
	
	view.add(statusIndicator);
	view.add(idLabel);
	view.add(releaseDateLabel);
	view.add(createdDateLabel);
	view.add(mainPhrasesCount);
	view.add(fortunePhrasesCount);
	view.add(groupsCount);
	//if (data.released == false) 
	view.add(releaseButton);
	view.add(deleteButton);
	
	
	function Refresh(newData){
		if(newData.released == true) statusIndicator.backgroundColor = globals.ui.colors.green;
		else if(newData.released == false) statusIndicator.backgroundColor = globals.ui.colors.orange;
		
		idLabel.text = "Release: "+newData.releaseID;
		
		var releaseDateText = "-"; 
		if (newData.releaseDate != null) releaseDateText = new moment(data.releaseDate).format("dd, D/M/YY");
		releaseDateLabel.text = "RD: "+releaseDateText;
		createdDateLabel.text = "CD: "+new moment(newData.createdDate).format("dd, D/M/YY");
		
		mainPhrasesCount.text = "MainP: "+data.mainPhrasesIDs.length;
		fortunePhrasesCount.text = "FortuneP: "+data.fortunePhrasesIDs.length;
	}
	
	Refresh(data);
	
	
	return view;
}