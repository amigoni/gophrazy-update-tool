exports.createWin = function(){
	var win = Titanium.UI.createWindow({  
		backgroundColor: globals.ui.colors.black,
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    fullscreen: true,
	    navBarHidden: true,
	    opacity: 1,
	});
	
	function OpenWindow(name, data){
		var module;
		if(name == "mainMenuWin") module = require("ui/MainMenuWin");
		else if(name == "languageWin") module = require("ui/LanguageWin");
		else if(name == "phrasesList") module = require("ui/PhrasesListWin");
		else if(name == "phraseDetail") module = require("ui/PhraseDetailWin");
		else if(name == "groupsList") module = require("ui/GroupsListWin");
		else if(name == "groupDetail") module = require("ui/GroupDetailWin");
		else if(name == "releasesList") module = require("ui/ReleasesListWin");
		else if(name == "realisingPopUp") module = require("ui/ReleasingPopUp");
		else if(name == "listPopUpWin") module = require("ui/common/ListPopUpWin");
		else if(name == "broadcastWin") module = require("ui/BroadcastWin");
		
		var win1 = module.loadWin(data);
		win1.addEventListener("closed", function(){
			globals.ui.rootWin.remove(win1);
			win1 = null;
		});
		
		globals.ui.rootWin.add(win1);
		win1.Open();
	}
	
	win.OpenWindow = OpenWindow;
	
	globals.ui.rootWin = win;
		
	
	return win;
};
