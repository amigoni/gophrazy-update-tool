exports.loadWin = function(winData){
	var data = winData.data;
	var isNew = winData.isNew;
	
	var Window = require("ui/common/Window");
	var NavBar = require("ui/common/NavBar");
	var TextField = require("ui/common/TextField");
	var LineItem = require("ui/common/LineItem");
	var PhrasesController = require("controller/PhrasesController");
	var GroupsController = require("controller/GroupsController");
	var ReleasesController = require("controller/ReleasesController");
	var SquareButton = require("ui/common/SquareButton");
	
	var view = Window.createView();
	var navBar = NavBar.createView("Back", "Phrase Detail", "Save");
	
	var changed = false;
	
	if (isNew == true){
		data = {};
		data.text = "";
		data.author = "";
		data.description = "";
		data.includeInGeneral = 1;
		data.isFunny = 0;
		data.type = "misc";
		data.released = "false";
		data.groupID = null;
	}
	
	navBar.leftButton.addEventListener("singletap", function(){
		if (changed == true){
			var dialog = Ti.UI.createAlertDialog({
		    	cancel: 2,
		    	buttonNames: ['Yes', 'No', 'Cancel'],
		    	//message: '',
		    	title: 'Save Changes?'
			});
			
			dialog.addEventListener('click', function(e){
			    if (e.index === e.source.cancel){
			      Ti.API.info('The cancel button was clicked');
			    }
			    
			    if(e.index == 0){
			    	Save();
			    }
			    else if (e.index == 1){
			    	Close();
					view.fireEvent("closeWin");
			    }
			});
			dialog.show();
			changed = false;
		}
		else{
			Close();
			view.fireEvent("closeWin");
		}
	});
	
	navBar.rightButton.addEventListener("singletap", function(){
		Save();
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: globals.platform.width,
		height: (globals.platform.actualHeightNumber-44).toString()+"dp",
		contentWidth: globals.platform.width,
		height: "auto",
		top: "44dp"
	});
	
	var container = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: "0dp",
		layout: "vertical"
	});
	
	var statusBarColor = globals.ui.colors.red;
	if(data.released == "true") statusBarColor = globals.ui.colors.green;
	else if(data.released == "staged") statusBarColor = globals.ui.colors.orange;
	var statusBar = Ti.UI.createView({backgroundColor: statusBarColor, width:"280dp", height: "4dp"});
	
	var textTextArea = TextField.createView("Text:", data.text, "50dp");
	textTextArea.top = "10dp";
	textTextArea.textArea.addEventListener("focus", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
	});
	
	textTextArea.textArea.addEventListener("change", function(e){
		changed = true;
		charsLabel.text = "Chars: "+textTextArea.textArea.value.length;
		var wordsArray = textTextArea.textArea.value.match(/\S+\s/g);
		if(wordsArray == null) wordsArray = [];
		wordsLabel.text = "Words: "+wordsArray.length;
		difficultyLabel.text = "Diff: "+wordsLabel.text;	
		
		if(e.value.length > 2) {
			autoCompleteTableView.visible = true;
			autoCompleteTableView.update(e.value);
		}
		else{
			autoCompleteTableView.visible = false;
		}	
	});
	
	textTextArea.textArea.addEventListener("singletap", function(){
		
	});
	
	var textStatsContainer = Ti.UI.createView({
		width: "280dp",
		height: "20dp",
		left: "20dp",
		layout: "horizontal"
	});
	
	var charsLabel = Ti.UI.createLabel({
		width: Ti.UI.SIZE,
		height: "20dp",
		//top: "0dp",
		left: "0dp",
		text: "Chars: "+data.text.length,
		textAlign: "left",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.lightGrey
	});
	
	var wordsLength = 0;
	if(data.text != null && data.text.length > 0) wordsLength = data.text.match(/\S+\s/g).length;
	var wordsLabel = Ti.UI.createLabel({
		width: Ti.UI.SIZE,
		height: "20dp",
		//top: "0dp",
		left: "10dp",
		text: "Words: "+wordsLength,
		textAlign: "left",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.lightGrey
	});
	
	var difficultyLabel = Ti.UI.createLabel({
		width: Ti.UI.SIZE,
		height: "20dp",
		//top: "0dp",
		left: "10dp",
		text: "Diff: "+wordsLength,
		textAlign: "left",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.lightGrey
	});
	
	textStatsContainer.add(charsLabel);
	textStatsContainer.add(wordsLabel);
	textStatsContainer.add(difficultyLabel);
	
	
	var authorTextArea = TextField.createView("Author:", data.author, "35dp");
	authorTextArea.top = "10dp";
	authorTextArea.textArea.addEventListener("focus", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
	});
	
	var descriptionTextArea = TextField.createView("Description:", data.description, "70dp");
	descriptionTextArea.top = "10dp";
	descriptionTextArea.textArea.addEventListener("focus", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
	});
	
	
	var bottomContainer = Ti.UI.createView({
		width: "280dp",
		height: Ti.UI.SIZE
	});
	
	var bottomLeftContainer = Ti.UI.createView({
		width: "135dp",
		top: "0dp",
		left: "0dp",
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var phraseIDLine = LineItem.createView("PhraseID:", data.phraseID, "static");
	
	var createdDateText; 
	if (data.createdDate != null) createdDateText = new moment(data.createdDate).format("dd, D/M/YY");
	var createdLine = LineItem.createView("Created:", createdDateText , "static");
	createdLine.value = data.createdDate;
	
	var includeInGeneral = LineItem.createView("InGeneral:", data.includeInGeneral, "boolean");
	includeInGeneral.rightLabel.addEventListener("singletap", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
		
		if(typeLine.value == "fortuneCookie" && includeInGeneral.value == true){
			typeLine.value = null;
			typeLine.rightLabel.text = "null";
		}
		
		if(includeInGeneral.value == true){
			groupIDLine.value == null;
			groupIDLine.rightLabel.text = "null";
		}
	});
	
	var isFunny = LineItem.createView("IsFunny:", data.isFunny, "boolean");
	isFunny.rightLabel.addEventListener("singletap", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
	});
	
	bottomLeftContainer.add(phraseIDLine);
	bottomLeftContainer.add(createdLine);
	bottomLeftContainer.add(includeInGeneral);
	bottomLeftContainer.add(isFunny);
	
	
	var bottomRightContainer = Ti.UI.createView({
		width: "135dp",
		top: "0dp",
		right: "0dp",
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var group = {name: null};
	if (data.groupID != null) group = globals.col.groups.find({groupID: data.groupID})[0];
	if(group == null) group = {};	
	var groupIDLine = LineItem.createView("GroupID:", group.groupID, "list");
	groupIDLine.value = group.groupID;
	
	groupIDLine.rightLabel.addEventListener("singletap", function(){
		if (data.released != "true")
		{
			changed = true;
			var groups = JSON.parse(JSON.stringify(GroupsController.GetGroups("unreleased")));
			//groups.unshift({name:"fortune_cookie"});
			//groups.unshift({name:"main"});
			groups.unshift({name: null});
	
			function callback(rowData){
				group = globals.col.groups.find({groupID: rowData.groupID})[0];
				if(group != null){
					groupIDLine.value = rowData.groupID;
					releasedLine.rightLabel.text = group.released;
					releasedLine.value = group.released;
					groupIDLine.rightLabel.text = rowData.groupID;
					
					includeInGeneral.value = false;
					includeInGeneral.rightLabel.text = "false";
					
					if( typeLine.value == "fortuneCookie"){
						typeLine.value == null;
						typeLine.rightLabel.text = "null";
					}
					
					UpdateReleaseStatus({releaseID: group.releaseID, released: group.released});
				}
				else{
					groupIDLine.value = null;
					groupIDLine.rightLabel.text = "null";
					UpdateReleaseStatus({releaseID: null, released: "false"});
				} 
			}
			
			globals.ui.rootWin.OpenWindow("listPopUpWin", {data: groups, name: "Groups", callback: callback});
		}
		else alert("This Phrase was already released.");
	});
	
	var releasedLine = LineItem.createView("Released:", data.released, "static");
	releasedLine.rightLabel.color = statusBarColor;
	
	var releaseIDLine = LineItem.createView("ReleasedID:", data.releaseID, "list");
	releaseIDLine.rightLabel.color = statusBarColor;
	releaseIDLine.rightLabel.addEventListener("singletap", function(){
		if(data.released != "true")
		{
			changed = true;
			if(groupIDLine.value == null || groupIDLine.value == "main" || groupIDLine.value == "fortuneCookie")
			{
				var releases = JSON.parse(JSON.stringify(ReleasesController.GetReleases("unreleased")));
				for(var i =0; i < releases.length; i++){
					releases[i].name = releases[i].releaseID;
				}
				releases.unshift({name: null});
						
				function callback(rowData){
					UpdateReleaseStatus(rowData);
				}
				
				globals.ui.rootWin.OpenWindow("listPopUpWin", {data: releases, name: "Releases", callback: callback});
			}
			else alert("ReleaseID is set by GroupID");
		}
		else alert("This Phrase was already released.");
	});
	
	
	var typeLine = LineItem.createView("Type:", data.type, "list");
	typeLine.rightLabel.addEventListener("singletap", function(){
		if (data.released != "true"){
			changed = true;
			//OpenListPicker
			var options = [
				{name: null},
				{name:"misc"},
				{name:"fortuneCookie"},
				{name:"tutorial"}
			];
			
			function callback(value){
				typeLine.value = value.name;
				typeLine.rightLabel.text = value.name;
				
				if(value.name == "fortuneCookie"){
					includeInGeneral.value = false;
					includeInGeneral.rightLabel.text = "false";
				}
				else if(value.name == "misc" && groupIDLine.value == null){
					includeInGeneral.value = true;
					includeInGeneral.rightLabel.text = "true";
				}
				
				if(value.name == "fortuneCookie"){
					groupIDLine.value == null;
					groupIDLine.rightLabel.text = "null";
				}
			}
			
			globals.ui.rootWin.OpenWindow("listPopUpWin", {data: options, name: "Type", callback: callback});
		}
		else alert("This Phrase was already released.");
	});
	
	bottomRightContainer.add(groupIDLine);
	bottomRightContainer.add(releasedLine);
	bottomRightContainer.add(releaseIDLine);
	bottomRightContainer.add(typeLine);
	
	bottomContainer.add(bottomLeftContainer);
	bottomContainer.add(bottomRightContainer);
	
	
	var buttonsContainer = Ti.UI.createView({
		width: "280dp",
		top: "10dp",
		height: Ti.UI.SIZE
	});
	
	var deleteButton = SquareButton.createView("Delete");
	deleteButton.addEventListener("singletap", function(){
		if (data.released != "true"){
			PhrasesController.DeletePhrase(data.phraseID);
			Ti.App.fireEvent("deletePhrase");
			view.fireEvent("closeWin");
			alert(data.phraseID+" deleted");
		}
		else alert("This Phrase was already released.");
	});
	
	if(isNew == false) buttonsContainer.add(deleteButton);
	
	
	
	container.add(statusBar);
	container.add(textTextArea);
	container.add(textStatsContainer);
	container.add(authorTextArea);
	container.add(descriptionTextArea);
	container.add(bottomContainer);
	container.add(buttonsContainer);
	
	scrollView.add(container);
	
	var autoCompleteTableView = exports.CreateAutoCompleteTableView(globals.col.phrases);
	autoCompleteTableView.addEventListener("singletap", function(e){
		textTextArea.textArea.value = globals.col.phrases.find({$id:e.rowData.$id})[0].text;
		autoCompleteTableView.visible = false;
	});
	
	view.add(navBar);
	view.add(scrollView);
	view.add(autoCompleteTableView);
	
	
	function Save(){
		var oldGroupID = data.groupID;
		var oldReleaseID = data.releaseID;
		
		var newData = {};
		if(isNew == false) newData = globals.col.phrases.find({$id:data.$id})[0];
		
		newData.text = textTextArea.textArea.value.trim();
		newData.text += " ";
		var wordsArray = [];
		if(newData.text.length > 1) wordsArray = newData.text.match(/\S+\s/g);	
		
		newData.length = newData.text.length;
		newData.numberOfWords = wordsArray.length;
		newData.difficulty = newData.numberOfWords;
		newData.author = authorTextArea.textArea.value;
		newData.description = descriptionTextArea.textArea.value;
		newData.includeInGeneral = includeInGeneral.value;
		newData.isFunny = isFunny.value;
		newData.type = typeLine.rightLabel.text;
		newData.released = releasedLine.value;
		newData.releaseID = releaseIDLine.value;
		newData.groupID = groupIDLine.value;

		if(isNew == false) globals.col.phrases.commit();
		else newData = PhrasesController.AddPhrase(newData);
		
		GroupsController.AddPhraseToGroup(newData, oldGroupID, newData.groupID);
		ReleasesController.AddPhraseToRelease(newData, oldReleaseID, newData.releaseID);
		
		Ti.API.info("SAVING PHRASE");
		Ti.API.info(newData);
		
		Close();
		view.fireEvent("closeWin");
		
		if(isNew == false) Ti.App.fireEvent("updatePhrase", {$id:newData.$id});
		else Ti.App.fireEvent("addPhrase", {data:newData});
	}
	
	function UpdateReleaseStatus(rowData){
		if(rowData.releaseID != null){
			releaseIDLine.value = rowData.releaseID;
			releaseIDLine.rightLabel.text = rowData.releaseID;
			releaseIDLine.rightLabel.color = globals.ui.colors.orange;
			
			releasedLine.value = "staged";
			releasedLine.rightLabel.text = "staged";
			releasedLine.rightLabel.color = globals.ui.colors.orange;	
			
			statusBar.backgroundColor = globals.ui.colors.orange;	
		}
		else{
			releaseIDLine.value = null;
			releaseIDLine.rightLabel.text = "null";
			releaseIDLine.rightLabel.color = globals.ui.colors.red;
			
			releasedLine.value = "false";
			releasedLine.rightLabel.text = "false";
			releasedLine.rightLabel.color = globals.ui.colors.red;	
			
			statusBar.backgroundColor = globals.ui.colors.red;
		}
	}
	
	function Close(){
		
	}
	
	
	return view;
};


exports.CreateAutoCompleteTableView = function (table, clickCallBack){
	var tableData = [];
	var data1 = table.find({deleted:false});//table.find({text: new RegExp(value,"i")} );
	var tableView = Titanium.UI.createTableView({
		backgroundColor: globals.ui.colors.black,
		width: "280dp",
		height: "140dp",
		top: "130dp",//480,
		separatorStyle: 1,
		visible: false,
		zIndex: 1,
		opacity: 0.9
	});	
	
	//tableView.setContentInsets({bottom: 200},{animated:false});
		
	tableView.addEventListener('click', function(e){
		//tableView.fireEvent
	});
	
	tableView.update = function (value){
		value = value.toLowerCase();
		if (value.length > 2){
			var dataFound = [];
				
			for(var i = 0; i < data1.length; i++){
				//var patt = "/"+value+"/";
				//if(patt.test(data1[i].text) == true) dataFound.push(data1[i]);
				var string = data1[i].text.toLowerCase();
				if(string.search(value) != -1) dataFound.push(data1[i]);
			}
			
			
			//Sorting function splits the results that start with the entry first then the ones that contain it. 
			function sortInputFirst(input, data) {
			    var first = [];
			    var others = [];
			    if (input.length > 0){
			    	 for (var i = 0; i < data.length; i++) {
				       var inputL = input.toLowerCase(); //making input lower case
				       var nameL = data[i].text.toLowerCase(); //making data lower case
				       
				        if (nameL.indexOf(inputL) == 0) {
				            first.push(data[i]);
				        } else {
				            others.push(data[i]);
				        };
				    };
				    
				    //Sorting object Function 
				    function sortAZ(ob1,ob2) {
				    	var n1 = ob1.text.toLowerCase();
				    	var n2 = ob2.text.toLowerCase();
				    	if (n1 > n2) {return 1;}
						else if (n1 < n2){return -1;}
					    else { return 0;}//nothing to split
					};
				    
				    first.sort(sortAZ);
				    others.sort(sortAZ);
				    
				    return(first.concat(others));
			    }else{
			    	return [];
			    };
			};
				
			var rows = sortInputFirst(value, dataFound);
			
			//Ti.API.info(rows.length);
			
			//make the table invisible when there is nothing in the field or no results
			if (value  && rows.length > 0 ){tableView.visible = true;}
			else {tableView.visible = false;}
			
			var tableData = [];      
		 	for(var i = 0; i < rows.length; i++){       
	            tableData.push(createRow(rows[i]));                 
		   	}; 
		   	
		   	function createRow(rowData){
	   			var row = Titanium.UI.createTableViewRow({ 
		        	className: 'searchResultsRow',
		        	backgroundColor:'transparent',
		        	selectionStyle: 1,
		            left: 10,
		            height: 35,
		            kind: 'autocomplete',
		            title: rowData.text,
		            value: rowData.text,
		            $id: rowData.$id,
		            font: globals.ui.fonts.hel12r,
		            color: globals.ui.colors.white,
		            data: rowData
		        });
		        
		        return row;
	   		};	
		}
		else {
			tableData = [];
			tableView.visible = false;
		}	
		
		tableView.data = tableData; 
	};
	
	/*
	tableView.appear = function (){
		tableView.opacity = .01;
		tableView.visible = true;
		//setTimeout(function (){tableView.visible = true},300);
		tableView.animate(Ti.UI.createAnimation({
			opacity:1,
			duration: 300,
			//top: 58,
			height:147,
			delay:350
		}));
	};
	
	tableView.disappear = function (){
		setTimeout(function (){tableView.visible = false;},150);
		tableView.animate(Ti.UI.createAnimation({
			//anchorPoint:{x:0, y:0},
			opacity:1,
			duration: 150,
			//top:480
			height:1
			//top:250
		}));
	};	
*/
			
	return tableView;
};
