exports.loadWin = function(){
	var Window = require("ui/common/Window");
	var NavBar = require("ui/common/NavBar");
	
	var view = Window.createView();
	var navBar = NavBar.createView("Back", "Groups", "New");
	
	navBar.leftButton.addEventListener("singletap", function(){
		Close();
		view.fireEvent("closeWin");
	});
	
	navBar.rightButton.addEventListener("singletap", function(){
		globals.ui.rootWin.OpenWindow("groupDetail", {data: null, isNew: true});
	});
	
	
	var tableView = CreateTableView();
	
	
	view.add(navBar);
	view.add(tableView);
	
	function Close(){
		
	}
	
	
	return view;
};


function CreateTableView(){
	var Common = require("ui/common/Common");
	var GroupsController = require("controller/GroupsController");
	
	var data = GroupsController.GetGroups();
	Ti.API.info(data);
	
	var tableData = [];
	
	var view = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		separatorColor: globals.ui.colors.white,
		height: (globals.platform.actualHeightNumber-44).toString()+"dp",
		top: "44dp"
	});
	
	//Common.MakeDynamicTableView(view,)
	
	function Refresh(){
		tableData = [];
		for(var i = 0; i < data.length; i++){
			var row = CreateRow(data[i]);
			tableData.push(row);
		}
		
		view.data = tableData;
	}
	
	function UpdateGroup(data){
		for(var i = 0; i < tableData.length; i++){
			if(tableData[i].data.$id == data.$id){
				tableData[i].Update();
			}
		}
	}
	
	function AddGroup(data){
		tableData.unshift(CreateRow(data.data));
		view.data = tableData;
	}
	
	function DeleteGroup(data){
		for(var i = 0; i < tableData.length; i++){
			if(tableData[i].data.$id == data.$id){
				tableData.splice(i,1);
				break;
			}
		}
		view.data = tableData;
	}
	
	function Close(){
		Ti.App.removeEventListener("updatePhrase", UpdatePhrase);
		Ti.App.removeEventListener("addPhrase", AddPhrase);	
		Ti.App.removeEventListener("deleteGroup", AddGroup);	
	}
	
	view.Refresh = Refresh;
	view.Close = Close;
	
	Ti.App.addEventListener("updateGroup", UpdateGroup);	
	Ti.App.addEventListener("addGroup", AddGroup);	
	Ti.App.addEventListener("deleteGroup", DeleteGroup);	
	
	Refresh();
	
	
	return view;
}


function CreateRow(data){
	var view = Ti.UI.createTableViewRow({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height: "40dp",
		selectionStyle: false,
		data: data
	});
	
	var background = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	background.addEventListener("singletap", function(){
		globals.ui.rootWin.OpenWindow("groupDetail", {data: data, isNew: false});
	});
	
	var square = Ti.UI.createView({
		backgroundColor: globals.ui.colors.white,
		width: "3dp",
		height: "36dp",
		left: "4dp",
		top: "2dp"
	});
	
	var nameLabel = Ti.UI.createLabel({
		width: "280dp",
		height: Ti.UI.SIZE,
		top: "2dp",
		left: "15dp",
		text: data.name,
		textAlign: "left",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.white
	});
	
	var numberOfPhrases = Ti.UI.createLabel({
		width: "100dp",
		height: "15dp",
		bottom: "5dp",
		left: "15dp",
		text: "Phrases: "+data.phrasesIDs.length,
		textAlign: "left",
		font: globals.ui.fonts.hel11b,
		color: globals.ui.colors.white
	});
	
	
	var releaseDateLabel = Ti.UI.createLabel({
		width: "150dp",
		height: "15dp",
		top: "2dp",
		right: "10dp",
		text: "RD: ",
		textAlign: "right",
		font: globals.ui.fonts.hel11b,
		color: globals.ui.colors.white
	});
	
	var createdDateLabel = Ti.UI.createLabel({
		width: "150dp",
		height: "15dp",
		bottom: "2dp",
		right: "10dp",
		text: "CD: "+new moment(data.createdDate).format("dd, D/M/YY"),
		textAlign: "right",
		font: globals.ui.fonts.hel11b,
		color: globals.ui.colors.white
	});
	
	
	view.add(background);
	view.add(square);
	view.add(nameLabel);
	view.add(numberOfPhrases);
	view.add(releaseDateLabel);
	view.add(createdDateLabel);
	
	
	function Update(){
		view.data = globals.col.groups.find({groupID: view.data.groupID})[0];
		Refresh(view.data);
	}
	
	function Refresh(newData){
		if(newData.released == "true") square.backgroundColor = globals.ui.colors.green;
		else if(newData.released == "false") square.backgroundColor = globals.ui.colors.red;
		else if(newData.released == "staged") square.backgroundColor = globals.ui.colors.orange;
		
		nameLabel.text = newData.groupID+" - "+newData.name;
		
		var releaseDateText = "-"; 
		if (newData.releaseDate != null) releaseDateText = new moment(data.releaseDate).format("dd, D/M/YY");
		releaseDateLabel.text = "RD: "+releaseDateText;
		createdDateLabel.text = "CD: "+new moment(newData.createdDate).format("dd, D/M/YY");
		
		numberOfPhrases.text = "Phrases: "+data.phrasesIDs.length;
	}
	
	view.Update = Update;
	
	Refresh(view.data);
	
	
	return view;
}
