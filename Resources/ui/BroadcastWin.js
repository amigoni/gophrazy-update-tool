exports.loadWin = function(){
	var BroadcastController = require("controller/BroadcastController");
	var Window = require("ui/common/Window");
	var NavBar = require("ui/common/NavBar");
	var TextField = require("ui/common/TextField");
	
	var view = Window.createView();
	var navBar = NavBar.createView("Back", "Broadcast Notification", "Send");
	
	navBar.leftButton.addEventListener("singletap", function(){
		Close();
		view.fireEvent("closeWin");
	});
	
	navBar.rightButton.addEventListener("singletap", function(){
		BroadcastController.BroadcastCustomMessage(messageTextArea.textArea.value, channelTextArea.textArea.value, function(e){
			if(e == "Success"){
				Close();
				view.fireEvent("closeWin");
			}
			else{
				
			}
			alert(e);
		});
		
	});
	
	var container = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: "55dp",
		layout: "vertical"
	});
	
	var channelTextArea = TextField.createView("Channel:", "GoPhrazyGlobal_en", "35dp");
	channelTextArea.top = "10dp";
	channelTextArea.textArea.addEventListener("change", function(e){
		
	});
	
	var messageTextArea = TextField.createView("Message:", "", "70dp");
	messageTextArea.top = "10dp";
	messageTextArea.textArea.addEventListener("change", function(e){
		if(e.value.length > 82) messageTextArea.textArea.value = e.value.substring(0,82);
	});
	
	container.add(channelTextArea);
	container.add(messageTextArea);
	
	view.add(navBar);
	view.add(container);
	
	
	function Close(){
		
	}
	
	
	return view;
};
