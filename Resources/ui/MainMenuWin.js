exports.loadWin = function(){
	var Window = require("ui/common/Window");
	var NavBar = require("ui/common/NavBar");
	
	var view = Window.createView();
	var navBar = NavBar.createView(null, "Main Menu", globals.language);
	
	navBar.leftButton.addEventListener("singletap", function(){
		alert("Left");
		Close();
		view.fireEvent("closeWin");
	});
	
	navBar.rightButton.addEventListener("singletap", function(){
		alert("Right");
	});
	
	
	var centerContainer = Ti.UI.createView({
		width: globals.platform.widht,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var phrasesButton = CreateButton("Phrases");
	phrasesButton.button.addEventListener("singletap", function(){
		globals.ui.rootWin.OpenWindow("phrasesList", null);
	});
	
	var groupsButton = CreateButton("Groups");
	groupsButton.button.addEventListener("singletap", function(){
		globals.ui.rootWin.OpenWindow("groupsList", null);
	});
	
	var releasesButton = CreateButton("Releases");
	releasesButton.button.addEventListener("singletap", function(){
		globals.ui.rootWin.OpenWindow("releasesList", null);
	});
	
	var broadcastButton = CreateButton("Broadcast");
	broadcastButton.button.addEventListener("singletap", function(){
		globals.ui.rootWin.OpenWindow("broadcastWin", null);
	});
	
	centerContainer.add(phrasesButton);
	centerContainer.add(groupsButton);
	centerContainer.add(releasesButton);
	centerContainer.add(broadcastButton);
	
	
	view.add(navBar);
	view.add(centerContainer);
	
	function Refresh(){
		var totalPhrases = globals.col.phrases.count({deleted: false});
		var totalMain = globals.col.phrases.count({deleted: false, includeInGeneral: true});
		var totalFortune = globals.col.phrases.count({deleted: false, type: "fortuneCookie"});
		
		var unreleasedPhrases = globals.col.phrases.count({deleted: false, released: "false"});
		var unreleasedMain = globals.col.phrases.count({deleted: false, released: "false", includeInGeneral: true});
		var unreleasedFortune = globals.col.phrases.count({deleted: false, released: "false", type: "fortuneCookie"});
		
		phrasesButton.subtitleLabel.text = "TotP: "+totalPhrases+"   TotM: "+totalMain+"   TotF: "+totalFortune+"\nUnP: "+unreleasedPhrases+"   UnM: "+unreleasedMain+"   UnF: "+unreleasedFortune;
	}
	
	function Close(){
		
	}
	
	Refresh();
	
	return view;
};



function CreateButton(name, subtitle){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 10
	});
	
	var button = Ti.UI.createLabel({
		//backgroundColor: globals.ui.colors.black,
		borderColor: globals.ui.colors.white,
		borderWidth: "1dp",
		width: "150dp",
		height: "50dp",
		text: name,
		textAlign: "center",
		verticalAlign: "center",
		font: globals.ui.fonts.gro20,
		color: globals.ui.colors.white
	});
	
	var subtitleLabel = Ti.UI.createLabel({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		top: "5dp",
		text: subtitle,
		textAlign: "center",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.white
	});
	
	view.add(button);
	view.add(subtitleLabel);
	
	view.subtitleLabel = subtitleLabel;
	view.button = button;
	
	
	return view;
}
