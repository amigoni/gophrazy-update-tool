exports.loadWin = function(winData){
	var data = winData.data;	
	var isNew = winData.isNew;
	var Window = require("ui/common/Window");
	var NavBar = require("ui/common/NavBar");
	var TextField = require("ui/common/TextField");
	var LineItem = require("ui/common/LineItem");
	var SquareButton = require("ui/common/SquareButton");
	var GroupsController = require("controller/GroupsController");
	var ReleasesController = require("controller/ReleasesController");
	var changed = false;
	
	if (isNew == true){
		data = {};
		data.name = "";
		data.description = "";
		data.bought = true;
		data.locked = false;
		data.featured = false;
		data.kind = "issue";
		data.released = "false";
		data.cost = "free";
		data.phrasesIDs = [];
	}
	
	var view = Window.createView();
	var navBar = NavBar.createView("Back", "Group Detail", "Save");
	
	navBar.leftButton.addEventListener("singletap", function(){
		if(changed == true){
			var dialog = Ti.UI.createAlertDialog({
		    	cancel: 2,
		    	buttonNames: ['Yes', 'No', 'Cancel'],
		    	//message: '',
		    	title: 'Save Changes?'
			});
			
			dialog.addEventListener('click', function(e){
			    if (e.index === e.source.cancel) Ti.API.info('The cancel button was clicked');
			    if(e.index == 0)Save();
			    else if (e.index == 1){
			    	Close();
					view.fireEvent("closeWin");
			    }
			});
			dialog.show();
		}
		else{
			Close();
			view.fireEvent("closeWin");
		} 
	});
	
	navBar.rightButton.addEventListener("singletap", function(){
		Save();
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: globals.platform.width,
		height: (globals.platform.actualHeightNumber-44).toString()+"dp",
		contentWidth: globals.platform.width,
		height: "auto",
		top: "44dp"
	});
	
	var container = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: "0dp",
		layout: "vertical"
	});
	
	var statusBarColor = globals.ui.colors.red;
	if(data.released == "true") statusBarColor = globals.ui.colors.green;
	else if(data.released == "staged") statusBarColor = globals.ui.colors.orange;
	var statusBar = Ti.UI.createView({backgroundColor: statusBarColor, width:"280dp", height: "4dp"});
	
	var nameTextArea = TextField.createView("Name:", data.name, "35dp");
	nameTextArea.top = "10dp";
	nameTextArea.textArea.addEventListener("focus", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
	});
	
	var descriptionTextArea = TextField.createView("Description:", data.description, "70dp");
	descriptionTextArea.top = "10dp";
	descriptionTextArea.textArea.addEventListener("focus", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
	});
	
	
	var bottomContainer = Ti.UI.createView({
		width: "280dp",
		height: Ti.UI.SIZE
	});
	
	var bottomLeftContainer = Ti.UI.createView({
		width: "135dp",
		top: "0dp",
		left: "0dp",
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var groupIDLine = LineItem.createView("GroupID:", data.groupID, "static");
	
	var createdDateText; 
	if (data.createdDate != null) createdDateText = new moment(data.createdDate).format("dd, D/M/YY");
	var createdLine = LineItem.createView("Created:", createdDateText , "static");
	createdLine.value = data.createdDate;
	
	var kindLine = LineItem.createView("Kind:", data.kind, "list");
	kindLine.rightLabel.addEventListener("singletap", function(){
		if (data.released != "true"){
			changed = true;
			//OpenListPicker
			var options = [
				{name: "null", value: null},
				{name:"issue", value: "issue"}
			];
			
			function callback(rowData){
				kindLine.value = rowData.value;
				kindLine.rightLabel.text = rowData.name;
			}
			
			globals.ui.rootWin.OpenWindow("listPopUpWin", {data: options, name: "Kind", callback: callback});
		}
		else alert("This Group was already released.");
	});
	var costLine = LineItem.createView("Cost:", data.cost, "static");
	
	bottomLeftContainer.add(groupIDLine);
	bottomLeftContainer.add(createdLine);
	bottomLeftContainer.add(kindLine);
	bottomLeftContainer.add(costLine);
	
	
	var bottomRightContainer = Ti.UI.createView({
		width: "135dp",
		top: "0dp",
		right: "0dp",
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var releasedLine = LineItem.createView("Released:", data.released, "static");
	
	var releaseIDLine = LineItem.createView("ReleasedID:", data.releaseID, "list");
	releaseIDLine.rightLabel.addEventListener("singletap", function(){
		if(data.released != "true"){
			changed = true;
		
			var releases = JSON.parse(JSON.stringify(ReleasesController.GetReleases("unreleased")));
			for(var i =0; i < releases.length; i++){
				releases[i].name = releases[i].releaseID;
			}
			releases.unshift({name: null});
					
			function callback(rowData){
				if(rowData.releaseID != null){
					releaseIDLine.value = rowData.releaseID;
					releaseIDLine.rightLabel.text = rowData.name;
					releaseIDLine.rightLabel.color = globals.ui.colors.orange;
					
					releasedLine.value = "staged";
					releasedLine.rightLabel.text = "staged";
					releasedLine.rightLabel.color = globals.ui.colors.orange;	
					
					statusBar.backgroundColor = globals.ui.colors.orange;	
				}
				else{
					releaseIDLine.value = null;
					releaseIDLine.rightLabel.text = "null";
					releaseIDLine.rightLabel.color = globals.ui.colors.red;
					
					releasedLine.value = "false";
					releasedLine.rightLabel.text = "false";
					releasedLine.rightLabel.color = globals.ui.colors.red;	
					
					statusBar.backgroundColor = globals.ui.colors.red;
				}
			}
			
			globals.ui.rootWin.OpenWindow("listPopUpWin", {data: releases, name: "Releases", callback: callback});
		}
		else alert("This Group was already released.");
	});
	
	var boughtLine = LineItem.createView("Bought:", data.bought, "boolean");
	boughtLine.addEventListener("singletap", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
	});
	var lockedLine = LineItem.createView("Locked:", data.locked, "boolean");
	lockedLine.addEventListener("singletap", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
	});
	var featuredLine = LineItem.createView("Featured:", data.featured, "boolean");
	featuredLine.addEventListener("singletap", function(){
		if(data.released == "true") alert("This Phrase was already released.");
		else changed = true;
	});
	
	bottomRightContainer.add(releasedLine);
	bottomRightContainer.add(releaseIDLine);
	bottomRightContainer.add(boughtLine);
	bottomRightContainer.add(lockedLine);
	bottomRightContainer.add(featuredLine);
	
	bottomContainer.add(bottomLeftContainer);
	bottomContainer.add(bottomRightContainer);
	
	
	var buttonsContainer = Ti.UI.createView({
		width: "280dp",
		top: "10dp",
		height: Ti.UI.SIZE
	});
	
	var deleteButton = SquareButton.createView("Delete");
	deleteButton.addEventListener("singletap", function(){
		if (data.released != "true"){
			GroupsController.DeleteGroup(data.groupID);
			Ti.App.fireEvent("deleteGroup", {$id: data.$id});
			view.fireEvent("closeWin");
			alert(data.name+" deleted");
		}
		else alert("This Group was already released.");
	});
	
	if(isNew == false) buttonsContainer.add(deleteButton);
	
	
	var phrasesContainer = Ti.UI.createView({
		width: "280dp",
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var phrasesCountLabel =  Ti.UI.createLabel({
		width: "70dp",
		height: "30dp",
		//top: "0dp",
		left: "0dp",
		text: "Phrases: "+data.phrasesIDs.length,
		textAlign: "left",
		font: globals.ui.fonts.gro12,
		color: globals.ui.colors.white,
		minimumFontSize: "8dp"
	});
	
	if(isNew != true){
		phrasesContainer.add(phrasesCountLabel);
		
		for(var i = 0; i < data.phrasesIDs.length; i++){
			var phrase = globals.col.phrases.find({phraseID: data.phrasesIDs[i]})[0];
			var row = CreatePhraseRow(phrase);
			row.addEventListener("singletap", function(){
				if(data.released == "true") alert("This Phrase was already released.");
				else changed = true;
			});
			
			row.addEventListener("removeRow", function(){
				phrasesContainer.remove(row);
				phrasesCountLabel.text = "Phrases: "+data.phrasesIDs.length;
			});
			phrasesContainer.add(row);
		}
	}
	
	
	container.add(statusBar);
	container.add(nameTextArea);
	container.add(descriptionTextArea);
	container.add(bottomContainer);
	container.add(buttonsContainer);
	container.add(phrasesContainer);
	
	scrollView.add(container);
	
	view.add(navBar);
	view.add(scrollView);
	
	
	function Save(){
		var oldReleaseID = data.releaseID;
		
		var newData = {};
		if(isNew == false) newData = globals.col.groups.find({$id:data.$id})[0];
		else newData.phrasesIDs = [];
		
		newData.name = nameTextArea.textArea.value;
		newData.description = descriptionTextArea.textArea.value;
		newData.bought = boughtLine.value;
		newData.locked = lockedLine.value;
		newData.featured = featuredLine.value;
		newData.kind = kindLine.value;
		newData.released = releasedLine.value;
		newData.cost = costLine.value;
		newData.releaseID = releaseIDLine.value;
		
		for(var i = 0; i < newData.phrasesIDs.length; i++){
			var phrase = globals.col.phrases.find({phraseID: newData.phrasesIDs[i]})[0];
			if(newData.releaseID != null) {
				phrase.releaseID = newData.releaseID;
				phrase.released = newData.released;
			}
			else{
				phrase.releaseID = null;
				phrase.released = "false";
			}		
		}
		
		globals.col.phrases.commit();

		if(isNew == false) globals.col.groups.commit();
		else newData = GroupsController.AddGroup(newData);
		
		ReleasesController.AddGroupToRelease(newData.groupID, oldReleaseID, newData.releaseID);
		
		Ti.API.info("SAVING GROUP: ");
		Ti.API.info(newData);
		
		Close();
		view.fireEvent("closeWin");
		
		if(isNew == false) Ti.App.fireEvent("updateGroup", {$id:newData.$id});
		else Ti.App.fireEvent("addGroup", {data:newData});
	}
	
	function Close(){
		
	}
	
	
	return view;
};


function CreatePhraseRow(data){
	var GroupsController = require("controller/GroupsController");
	var view = Ti.UI.createView({
		borderColor: globals.ui.colors.white,
		width: "280dp",
		height: "40dp",
		data: data
	});
	
	view.addEventListener("singletap", function(){
		var dialog = Ti.UI.createAlertDialog({
	    	cancel: 2,
	    	buttonNames: ['Open', 'Delete', 'Cancel'],
	    	//message: '',
	    	title: 'Phrase Menu'
		});
		
		dialog.addEventListener('click', function(e){
		    if (e.index === e.source.cancel){
		      Ti.API.info('The cancel button was clicked');
		    }
		    
		    if(e.index == 0){
		    	globals.ui.rootWin.OpenWindow("phraseDetail", {data: view.data, isNew: false});
		    }
		    else if (e.index == 1){
		    	GroupsController.AddPhraseToGroup(data, null);
		    	view.fireEvent("removeRow");
		    }
		});
		dialog.show();
	});
	
	var textLabel = Ti.UI.createLabel({
		width: "260dp",
		height: Ti.UI.SIZE,
		top: "5dp",
		left: "5dp",
		text: data.text,
		textAlign: "left",
		font: globals.ui.fonts.hel12r,
		color: globals.ui.colors.white
	});
	
	view.add(textLabel);
	
	
	return view;
};
