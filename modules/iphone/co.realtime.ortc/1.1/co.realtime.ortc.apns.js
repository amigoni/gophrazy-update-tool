/**
 *  co.realtime.ortc.apns.js - CommonJS wrapper to iOS co.realtime.ortc Titanium module, simplifying the APNS integration. 
 *
 * Note, that if you have placed the module file 'co.realtime.ortc.apns.js' in other directory than your caller code,
 * you will have to provide a relative path to the require call: 'var ORTC = require("../libs/co.realtime.ortc.apns");'
 *
 * For informations how to enable push notification with your Realtime account please follow the instructions at: 
 * http://messaging-public.realtime.co/documentation/starting-guide/mobilePushAPNS.html
 * 
 */

var ortc = require('co.realtime.ortc');

var wrap = {clusterUrl:null, url:null, connectionMetadata:null, announcementSubchannel:null, _devId:null};

wrap.connect = function(appKey, authToken){
	if(wrap.clusterUrl)
		ortc.clusterUrl = wrap.clusterUrl;
	if(wrap.url)
		ortc.url = wrap.url;
	if(wrap.connectionMetadata)
		ortc.connectionMetadata = wrap.connectionMetadata;
	if(wrap.announcementSubchannel)
		ortc.announcementSubchannel = wrap.announcementSubchannel;
	ortc.connect(appKey);
};

wrap.subscribeWithNotifications = function(channelName, subscribeOnReconnect){
	if(wrap._devId){
		ortc.setDeviceId(wrap._devId);
		ortc.subscribeWithNotifications(channelName, subscribeOnReconnect);
	} else {
		Titanium.Network.registerForPushNotifications({
		    types:[
		        Titanium.Network.NOTIFICATION_TYPE_BADGE,
		        Titanium.Network.NOTIFICATION_TYPE_ALERT,
		        Titanium.Network.NOTIFICATION_TYPE_SOUND
		    ],
		    success: function(e) {
		    	wrap._devId = e.deviceToken.toString();
		    	ortc.setDeviceId(wrap._devId);
		    	ortc.subscribeWithNotifications(channelName, subscribeOnReconnect);
		    },
		    error: function(e) {
		    	ortc.fireEvent('onException', {info:'APNS registration error: ' + e.message});
		    },
		    callback: function(e) {
		    	ortc.passNotification(e.data);
		    }
		});
	}
};

wrap.addEventListener = ortc.addEventListener;
wrap.disconnect = ortc.disconnect;
wrap.subscribe = ortc.subscribe;
wrap.unsubscribe = ortc.unsubscribe;
wrap.send = ortc.send;
wrap.isSubscribed = ortc.isSubscribed;
wrap.isConnected = ortc.isConnected;
wrap.presence = ortc.presence;

wrap.getHeartbeatTime = ortc.getHeartbeatTime;
wrap.setHeartbeatTime = ortc.setHeartbeatTime;
wrap.getHeartbeatFails = ortc.getHeartbeatFails;
wrap.setHeartbeatFails = ortc.setHeartbeatFails;
wrap.isHeartbeatActive = ortc.isHeartbeatActive;
wrap.enableHeartbeat = ortc.enableHeartbeat;
wrap.disableHeartbeat = ortc.disableHeartbeat;

module.exports = wrap;

